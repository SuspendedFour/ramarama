using UnityEngine;

class NetworkMain : MonoBehaviour {
    public static NetworkView instance;

    [RPC]
    public void CreateExplosion(Vector3 position, Vector3 startColorRgb, float startColorAlpha) {
        GameObject outerSplode = Lifecycle.Create("Explode");
        GameObject innerSplode = Lifecycle.Create("innerSplode");

        outerSplode.transform.position = position;
        innerSplode.transform.position = position;

        Color startColor = new Color(startColorRgb.x, startColorRgb.y, startColorRgb.z, startColorAlpha);
        innerSplode.GetComponent<ParticleSystem>().startColor = startColor;
        outerSplode.GetComponent<ParticleSystem>().startColor = startColor;

        //destroy explosion after it's done
        Lifecycle.Release(outerSplode, 2f);
        Lifecycle.Release(innerSplode, 2f);
    }
}