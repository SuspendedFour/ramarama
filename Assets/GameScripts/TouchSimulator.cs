using UnityEngine;
using System.Collections.Generic;

class TouchSimulator : MonoBehaviour {
    private Dictionary<string, SimTouch> touches = new Dictionary<string, SimTouch>();
    private string activeTouch = null;

    void Start() {
        touches["z"] = null;
        touches["x"] = null;
        touches["c"] = null;
        touches["v"] = null;
    }

    void Update() {
        if (Input.GetKeyDown("z")) {
            activeTouch = "z";
        }
        else if (Input.GetKeyDown("x")) {
            activeTouch = "x";
        }
        else if (Input.GetKeyDown("c")) {
            activeTouch = "c";
        }
        else if (Input.GetKeyDown("v")) {
            activeTouch = "v";
        }
        else if (Input.GetKeyDown("b")) {
            activeTouch = null;
        }

        foreach (SimTouch sim in new List<SimTouch>(touches.Values)) {
            if (sim != null) {
                RamaTouch touch = new RamaTouch(sim.position, sim.phase, sim.touchId);
                RamaInput.touches.Add(touch);

                if (sim.phase == TouchPhase.Began) {
                    sim.phase = TouchPhase.Moved;
                }
                else if (sim.phase == TouchPhase.Ended) {
                    touches[activeTouch] = null;
                }
            }
        }

        if (activeTouch == null) return;

        var simTouch = touches[activeTouch];

        if (Input.GetMouseButtonDown(1)) {
            if (simTouch == null) {
                var newTouch = new SimTouch();
                newTouch.phase = TouchPhase.Began;
                newTouch.position = Input.mousePosition;
                newTouch.touchId = RamaTouch.AllocateTouchID();
                touches[activeTouch] = newTouch;
            }
            else {
                simTouch.phase = TouchPhase.Ended;
            }
        }
        else if (simTouch != null && simTouch.phase == TouchPhase.Moved) {
            simTouch.position = Input.mousePosition;
        }
    }

    class SimTouch {
        public TouchPhase phase;
        public Vector2 position;
        public int touchId;
    }
}