﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;

class LiveBlock : MonoBehaviour {
    // lava=0, coral=1, robits=2, dessert=3
    public int nation { get { return team.nation; } }
    
    public Team team;

    public float health;
    public float initialHealth;
    private FMOD_StudioSystem soundSystem; // will point to overall sound system

    private Dictionary<GameObject, bool> collidedRocks = new Dictionary<GameObject, bool>();

	// Use this for initialization
	void Start () {
        this.gameObject.name = "LiveBlock";
        this.gameObject.tag = "Block";
        this.gameObject.AddComponent<ShadowBlock>();
        this.gameObject.AddOrGetComponent<Rigidbody2D>();
        this.gameObject.GetComponent<Collider2D>().isTrigger = false;
        this.gameObject.AddOrGetComponent<Gravity>();
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, .99f);

        this.rigidbody2D.isKinematic = false;
        this.gameObject.layer = LayerMask.NameToLayer("BlocksTeam" + team.teamNumber.ToString());
        //soundSystem = GameObject.Find("SoundSystem").GetComponent<initSoundSystem>().soundSystem;   // find sound system and attach to it
        //soundSystem.PlayOneShot("/movePieces/place", Vector3.zero); //TODO set positions

        Sprite sprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
        float area = Main.spriteToAreaRatio[sprite] * sprite.bounds.size.x * sprite.bounds.size.y;
        float mass = area * this.transform.localScale.x;
        this.rigidbody2D.mass = mass;
        this.health = mass * Constants.BLOCK_MASS_TO_HEALTH_FACTOR;
        this.initialHealth = this.health;

        //this.gameObject.AddComponent<HealthbarHost>();
        this.gameObject.AddComponent<FadeOutOnEndGame>();

        var longpress = this.gameObject.AddOrGetComponent<LongPress>();
        longpress.icon = Assets.pathToSprite["Timer/timer_pick"];
        longpress.touchThreshholdDistance = Constants.TOUCH_THRESHOLD_DISTANCE;
    }
	
	// Update is called once per frame
	void Update () {
	    //this.gameObject.GetComponent<HealthbarHost>().healthQuantile = this.health / this.initialHealth;
        var crack = this.GetComponent<CrackAnimation>();
        if (crack != null) {
            crack.healthQuantile = this.health / this.initialHealth;
        }
	}

    void OnCollisionEnter2D(Collision2D collision) {
        if (collidedRocks.ContainsKey(collision.gameObject)) return;

        if (collision.gameObject.tag == "Rock") {
            var rock = collision.gameObject.GetComponent<Rock>();
            this.collidedRocks[collision.gameObject] = true;

            float damage = rock.AttackDamage;
            if (this.team == rock.team) damage *= Constants.FRIENDLY_FIRE_DAMAGE_SCALE;
            health -= damage;

            //GameObject.Find("SoundSystem").GetComponent<initSoundSystem>().soundSystem.PlayOneShot("/Bangs/bang", Vector3.zero);   // find sound system and attach to it

            if (health < 0) {
                var scorePopup = Lifecycle.Create("ScorePopup");
                scorePopup.transform.position = this.transform.position;
                Lifecycle.Release(scorePopup, Constants.SCORE_POPUP_DURATION);

                if (this.team == rock.team) {
                    Scoreboard.AddScore(this.team, -Constants.BLOCK_REWARD);
                    scorePopup.GetComponent<ScorePopup>().Initialize(-Constants.BLOCK_REWARD, this.team.color);
                }
                else {
                    Scoreboard.AddScore(rock.team, Constants.BLOCK_REWARD);
                    scorePopup.GetComponent<ScorePopup>().Initialize(Constants.BLOCK_REWARD, rock.team.color);
                }

                PlayDeathSound();
                var explode = this.gameObject.AddComponent<Explode>();
                explode.ExplodeNow(this.gameObject.GetComponent<SpriteRenderer>().sprite);
                Destroy(this.gameObject);
            }
        }
    }

    void PlayDeathSound() {
        //switch (nation) // lava=0, coral=1, robits=2, dessert=3
        //{
        //    case 0:
        //        soundSystem.PlayOneShot("/Rocks/rockHit", Vector3.zero);
        //        break;
        //    case 1:
        //        soundSystem.PlayOneShot("/Waters/splash", Vector3.zero);
        //        break;
        //    case 2:
        //        soundSystem.PlayOneShot("/Computers/splode", Vector3.zero);
        //        break;
        //    case 3:
        //        //TODO fill sound
        //        soundSystem.PlayOneShot("/countdown/longbuzz", Vector3.zero);
        //        break;
        //}
    }

    void OnLongPress() {
        //soundSystem.PlayOneShot("/movePieces/pickup", Vector3.zero); //TODO set positions
        StartCoroutine("LongPressRoutine");
    }

    private IEnumerator LongPressRoutine() {
        // Wait 1 frame to prevent immediate LongPress callback in GhostBlock script 
        yield return null;
        Destroy(this.gameObject.GetComponent<LiveBlock>());
        Destroy(this.gameObject.GetComponent<Gravity>());
        Destroy(this.gameObject.GetComponent<ShadowBlock>());
        //Destroy(this.gameObject.GetComponent<HealthbarHost>());
        var ghostBlockScript = this.gameObject.AddComponent<GhostBlock>();
        ghostBlockScript.team = this.team;
        ghostBlockScript.JustSpawned();
    }

    void OnBuildPhaseEnd() {
        Destroy(this.gameObject.GetComponent<LongPress>());
        this.renderer.material = Assets.GetMaterial("Crack");
        this.gameObject.AddComponent<CrackAnimation>();
    }

    void OnBuildPhaseStart() {
        Destroy(this.gameObject);
    }
}
