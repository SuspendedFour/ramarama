﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class RamaInput : MonoBehaviour {

    public static List<RamaTouch> touches = new List<RamaTouch>();

    // Use this for initialization
    void Start () {
    
    }
    
    // Update is called once per frame
    void Update () {
        touches.Clear();

        RamaTouch mouseTouch = RamaTouch.FromMouse0();
        if (mouseTouch != null) {
            touches.Add(mouseTouch);
        }
        
        foreach (var touch in TuioInput.touches) {
            touches.Add(RamaTouch.FromTouch(touch));
        }
    }

    public static IEnumerable<RamaTouch> Touches { get { return touches; } }
}
