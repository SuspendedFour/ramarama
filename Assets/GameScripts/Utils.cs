﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Utils {

	public static Camera ScreenPointToCamera(Vector3 screenPoint) {
        Vector2 normalizedScreenPoint = new Vector2(screenPoint.x / Screen.width, screenPoint.y / Screen.height);
        return Camera.allCameras.FirstOrDefault((camera) => camera.rect.Contains(normalizedScreenPoint));
    }

    public static Vector3 ScreenPointToWorld(Vector3 screenPoint) {
        Camera cam = Utils.ScreenPointToCamera(screenPoint);
        if (cam == null) return new Vector3(999999, 99999, 999999);
        return cam.ScreenToWorldPoint(screenPoint - new Vector3(0, 0, cam.transform.position.z));
    }

    public static Vector3 ScreenPointToWorld(Camera camera, Vector3 screenPoint)
    {
        return camera.ScreenToWorldPoint(screenPoint - new Vector3(0, 0, camera.transform.position.z));
    }

    public static Vector2 XY(this Vector3 v) {
        return new Vector2 (v.x, v.y);
    }

    public static Vector3 SetXY(this Vector3 v, Vector2 xy)
    {
        return new Vector3(xy.x, xy.y, v.z);
    }

    public static Color SetAlpha(this Color color, float alpha) {
        return new Color(color.r, color.g, color.b, alpha);
    }

    public static void SetAlpha(this SpriteRenderer renderer, float alpha) {
        renderer.color = renderer.color.SetAlpha(alpha);
    }

    public static T AddOrGetComponent<T>(this GameObject gameObject) where T : Component {
        T component = gameObject.GetComponent<T>();
        return component == null ? gameObject.AddComponent<T>() : component;
    }

    public static Vector3 calculateScaleFactor(Sprite sprite, Vector2 targetSizeUnits) {
        // float spriteWidthUnits = sprite.texture.width / 100;
        // float ratio = pixels / Tiles.universePixelWidth;
        // return ratio * Tiles.universeUnitWidth / units;

        return new Vector3(targetSizeUnits.x / (sprite.texture.width / 100), targetSizeUnits.y / (sprite.texture.height / 100), 1);
    }

    // Program will crash if this fails
    public static List<Vector2> PlaceSquaresInRectOrDie(int squareCount, float squareRadius, Vector2 rectMin, Vector2 rectMax) {
        List<Vector2> result = new List<Vector2>();
        for (int i = 0; i < squareCount; i++) {

            Vector2 pos;
            findPosition:
                float x = Random.Range(rectMin.x + squareRadius, rectMax.x - squareRadius);
                float y = Random.Range(rectMin.y + squareRadius, rectMax.y - squareRadius);
                pos = new Vector2(x, y);
                foreach (var other in result) {
                    if (Vector2.Distance(pos, other) < squareRadius) {
                        goto findPosition;
                    }
                }

            result.Add(pos);
        }

        return result;
    }

    public static Color ColorFromHSV(float h, float s, float v, float a = 1)
    {
        // no saturation, we can return the value across the board (grayscale)
        if (s == 0)
            return new Color(v, v, v, a);

        // which chunk of the rainbow are we in?
        float sector = h / 60;

        // split across the decimal (ie 3.87 into 3 and 0.87)
        int i = (int)sector;
        float f = sector - i;

        float p = v * (1 - s);
        float q = v * (1 - s * f);
        float t = v * (1 - s * (1 - f));

        // build our rgb color
        Color color = new Color(0, 0, 0, a);

        switch(i)
        {
            case 0:
                color.r = v;
                color.g = t;
                color.b = p;
                break;

            case 1:
                color.r = q;
                color.g = v;
                color.b = p;
                break;

            case 2:
                color.r  = p;
                color.g  = v;
                color.b  = t;
                break;

            case 3:
                color.r  = p;
                color.g  = q;
                color.b  = v;
                break;

            case 4:
                color.r  = t;
                color.g  = p;
                color.b  = v;
                break;

            default:
                color.r  = v;
                color.g  = p;
                color.b  = q;
                break;
        }

        return color;
    }
        
    public static void ColorToHSV(Color color, out float h, out float s, out float v)
    {
        float min = Mathf.Min(Mathf.Min(color.r, color.g), color.b);
        float max = Mathf.Max(Mathf.Max(color.r, color.g), color.b);
        float delta = max - min;

        // value is our max color
        v = max;

        // saturation is percent of max
        if (!Mathf.Approximately(max, 0))
            s = delta / max;
        else
        {
            // all colors are zero, no saturation and hue is undefined
            s = 0;
            h = -1;
            return;
        }

        // grayscale image if min and max are the same
        if (Mathf.Approximately(min, max))
        {
            v = max;
            s = 0;
            h = -1;
            return;
        }

        // hue depends which color is max (this creates a rainbow effect)
        if (color.r == max)
            h = (color.g - color.b) / delta;            // between yellow & magenta
        else if (color.g == max)
            h = 2 + (color.b - color.r) / delta;        // between cyan & yellow
        else
            h = 4 + (color.r - color.g) / delta;        // between magenta & cyan

        // turn hue into 0-360 degrees
        h *= 60;
        if (h < 0 )
            h += 360;
    }
}
