﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {
    public static float WALL_THICKNESS = Tiles.PixelsToUnits(40f);

    private int wallType = 0;

    //0=no walls, 1=concrete, 2=rubber, 3=asteroids
    //TODO test no walls destruction of balls, reinit asteroids
    static public int NUM_TYPES_OF_WALLS = 3;  //4 after asteroids

	// Use this for initialization
	void Awake () {
        this.transform.localScale = new Vector3(1, 1, 1);
        this.GetComponent<BoxCollider2D>().size = new Vector2(WALL_THICKNESS, Tiles.spaceSize.x);
	}

    public void setWallType(int type)
    {
        this.wallType = type;
        gameObject.collider2D.enabled = type != 0;
        float wallFadeDuration = 4;

        switch (type)
        {
            case 1: //concrete
                gameObject.collider2D.isTrigger = false;
                gameObject.collider2D.sharedMaterial = Resources.Load("Prefabs/ConcreteWall") as PhysicsMaterial2D;
                gameObject.GetComponent<CrossFade>().BeginFade(Assets.pathToSprite["Walls/concreteleft"], wallFadeDuration);
                break;
            case 2: //rubber
                //gameObject.collider2D.sharedMaterial = Resources.Load("Prefabs/RubberWall") as PhysicsMaterial2D;
                gameObject.collider2D.isTrigger = true;
                gameObject.GetComponent<CrossFade>().BeginFade(Assets.pathToSprite["Walls/rubberleft"], wallFadeDuration);
                break;
            case 3: //asteroids
                gameObject.collider2D.isTrigger = true;
                gameObject.GetComponent<CrossFade>().BeginFade(Assets.pathToSprite["Walls/asteroidleft"], wallFadeDuration);
                break;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)  //this is currently only the "rubber wall" case
    {
        if (collision.gameObject.tag != "Rock") return;

        switch (wallType) {
            case 2:
                collision.gameObject.rigidbody2D.velocity = new Vector2(collision.gameObject.rigidbody2D.velocity.x, -collision.gameObject.rigidbody2D.velocity.y);
                break;
            case 3:
                Vector3 pos = collision.gameObject.transform.position;
                pos.y *= -1;
                collision.gameObject.transform.position = pos;
                break;
        }
        if (wallType==2 && collision.gameObject.tag == "Rock")
        {
        }
    }
}
