using UnityEngine;

class CameraZoom : MonoBehaviour {
    Vector3 previousMousePosition;

    void Update() {
        Camera camera = Utils.ScreenPointToCamera(Input.mousePosition);
        if (camera == null) return;

        // Update zoom
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0) {
            camera.orthographicSize -= scroll * 20;
        }

        // Update pan
        if (Input.GetMouseButton(2)) {
            Vector3 diff = Input.mousePosition - previousMousePosition;
            diff = camera.transform.rotation * diff;
            diff *= camera.orthographicSize / 100;
            Vector3 position = camera.transform.position - diff;
            camera.transform.position = camera.transform.position.SetXY(position);
        }

        previousMousePosition = Input.mousePosition;
    }
}