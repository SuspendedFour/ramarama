using UnityEngine;

class RamaClient : MonoBehaviour {
    private int port = Constants.port;
    // private string IPAddress = "128.54.107.43";
    private string IPAddress = "192.168.1.165";
    // private string IPAddress = "127.0.0.1";

    void Awake() {
        Assets.LoadAssets();
        Network.Connect(IPAddress, port);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
           Network.Connect(IPAddress, port);
        }
    }
}