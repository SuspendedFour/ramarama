﻿using UnityEngine;
using System;
using System.Collections;

public class RotationHandle : MonoBehaviour {

    private float initialZAxisRotation;
    private float initialYScale;
    private RotationController rotationController = new RotationController();
    private GameObject staticLayer;

    void Awake() {
        this.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["/controller2_rotate"];
        this.staticLayer = Lifecycle.Create("PlainOldSprite");
        this.staticLayer.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["/controller2_static"];
        this.staticLayer.transform.localScale = new Vector3(0, 0, 0);
    }

	void Update () {
        this.staticLayer.transform.position = this.transform.position.SetZ(Layers.RotationHandleStatic);
        this.staticLayer.transform.localScale = this.transform.lossyScale;
	}

    void OnTouchDown(UncapturedTouchEvent touch) {
        touch.Capture(this.gameObject);
        initialZAxisRotation = this.transform.parent.localRotation.eulerAngles.z;
        initialYScale = this.transform.parent.localScale.y;
        rotationController.StartRotation();
        rotationController.ProcessInput(GetTouchAngle(touch.position));
    }

    void OnTouchDrag(CapturedTouchEvent touch) {
        rotationController.ProcessInput(GetTouchAngle(touch.position));

        Vector3 scale = this.transform.parent.localScale;
        scale.y = rotationController.IsReflected ? -initialYScale : initialYScale;

        float zAxisRotation = initialZAxisRotation + rotationController.Z;
        
        this.transform.parent.localScale = scale;
        this.transform.parent.rotation = Quaternion.Euler(0, 0, zAxisRotation);
    }

    Angle GetTouchAngle(Vector2 touchWorld) {
        return new Angle(Quaternion.LookRotation(Vector3.forward, touchWorld - this.transform.parent.position.XY()).eulerAngles.z);
    }

    void OnDestroy() {
        Lifecycle.Release(staticLayer);
    }
}
