using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class LongPress : MonoBehaviour {

    public Sprite icon;

    // How far a touch needs to go before it's considered not to be a long touch
    public float touchThreshholdDistance = Constants.TOUCH_THRESHOLD_DISTANCE;

    private Dictionary<int, LongPressInstance> longPressHandlers = new Dictionary<int, LongPressInstance>();

    void OnTouchDown(UncapturedTouchEvent touch) {
        touch.Capture(this.gameObject);
        LongPressInstance handler = new LongPressInstance(this.gameObject, this.icon, this.touchThreshholdDistance);
        handler.OnTouchDown(touch);
        longPressHandlers[touch.touchId] = handler;

        StartCoroutine(handler.LongPressRoutine());
    }

    void OnTouchDrag(CapturedTouchEvent touch) {
        if (!longPressHandlers.ContainsKey(touch.touchId)) return;
        longPressHandlers[touch.touchId].OnTouchDrag(touch);
    }

    void OnTouchUp(CapturedTouchEvent touch) {
        if (!longPressHandlers.ContainsKey(touch.touchId)) return;

        longPressHandlers[touch.touchId].OnTouchUp();
        longPressHandlers.Remove(touch.touchId);
    }

    void OnDestroy() {
        foreach (var handler in longPressHandlers.Values) {
            handler.Destroy();
        }
    }

    private class LongPressInstance {
        private float startTime;
        private Vector2 objectToTouchOffset;
        private Vector2 initialTouch;
        private CapturedTouchEvent lastTouch;
        private bool ignoreCurrentTouch = true;

        private GameObject longPressTimer;
        private GameObject gameObject;
        private Sprite icon;
        private float touchThreshholdDistance;

        public LongPressInstance(GameObject gameObject, Sprite icon, float touchThreshholdDistance) {
            this.gameObject = gameObject;
            this.icon = icon;
            this.touchThreshholdDistance = touchThreshholdDistance;
        }

        public void OnTouchDown(UncapturedTouchEvent touch) {
            objectToTouchOffset = touch.position - this.gameObject.transform.position.XY();
            initialTouch = touch.position;
            startTime = Time.time;
            ignoreCurrentTouch = false;

            longPressTimer = Lifecycle.Create("LongPressTimer");
            longPressTimer.transform.localScale = new Vector3(10, 10, 1);
            longPressTimer.transform.Find("Icon").GetComponent<SpriteRenderer>().sprite = icon;
            longPressTimer.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            longPressTimer.transform.position = touch.position.SetZ(Layers.LongPressIcon);
            Tiles.OrientUpward(longPressTimer.transform);

            this.longPressTimer.renderer.material.SetFloat("_Angle", 0);
            this.longPressTimer.GetComponent<NetworkedSprite>().SetFloatProperty("_Angle", 0);
        }

        public void OnTouchDrag(CapturedTouchEvent touch) {
            if (ignoreCurrentTouch) return;

            Vector2 target = this.gameObject.transform.position.XY() + objectToTouchOffset;
            if (Vector2.Distance(touch.position, target) > touchThreshholdDistance) {
                this.CleanupTouch();
                this.gameObject.SendMessage("OnLongPressCancelled", SendMessageOptions.DontRequireReceiver);
                this.ignoreCurrentTouch = true;
            }
            else {
                this.longPressTimer.transform.position = target.SetZ(Layers.LongPressIcon);
                float t = (Time.time - startTime) / Constants.LONGPRESS_DURATION;
                this.longPressTimer.renderer.material.SetFloat("_Angle", t);
                this.longPressTimer.GetComponent<NetworkedSprite>().SetFloatProperty("_Angle", t);
            }

            this.lastTouch = touch;
        }

        public void OnTouchUp() {
            if (ignoreCurrentTouch) return;

            CleanupTouch();
            ignoreCurrentTouch = true;
        }

        public IEnumerator LongPressRoutine() {
            yield return new WaitForSeconds(Constants.LONGPRESS_DURATION);
            if (ignoreCurrentTouch) yield break;

            this.CleanupTouch();
            LongPressEventData eventData = new LongPressEventData();
            eventData.initialTouch = initialTouch;
            eventData.lastTouch = lastTouch;
            this.gameObject.SendMessage("OnLongPress", eventData, SendMessageOptions.DontRequireReceiver);
            this.ignoreCurrentTouch = true;
        }

        void CleanupTouch() {
            Lifecycle.Release(longPressTimer);
        }

        public void Destroy() {
            CleanupTouch();
        }
    }
}

class LongPressEventData {
    public Vector2 initialTouch;
    public CapturedTouchEvent lastTouch;
}