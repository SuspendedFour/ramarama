using UnityEngine;

class VelocityEditorView : MonoBehaviour {
    public Vector3 velocity;

    void Update() {
        this.velocity = this.rigidbody2D.velocity;
    }
}