using UnityEngine;

public class HealthbarHost : MonoBehaviour {
    public float healthQuantile;

    private static Object healthbarPrefab;

    private Transform healthbar;
    private Transform greenbar;
    private Transform redbar;
    private Vector3 offset;

    void Start() {
        if (healthbarPrefab == null) {
            healthbarPrefab = Resources.Load("Prefabs/Healthbar");
        }

        Vector3 up = Tiles.GetUpwardDirection(this.transform.position);
        var bounds = this.renderer.bounds;
        this.offset = bounds.center - this.transform.position - bounds.extents;
        this.offset.Scale(up);
        this.offset -= Constants.HEALTHBAR_OFFSET * up;

        this.healthbar = ((GameObject) Instantiate(healthbarPrefab)).transform;
        this.greenbar = this.healthbar.Find("Green");
        this.redbar = this.healthbar.Find("Red");

        this.healthbar.position = this.transform.position + this.offset;
        this.healthbar.localScale = new Vector3(Constants.HEALTHBAR_WIDTH, Constants.HEALTHBAR_HEIGHT, 1);
        this.healthbar.transform.rotation = Tiles.GetUpwardFacing(this.transform.position);

        this.greenbar.position = this.greenbar.position.SetZ(Layers.GreenHealthBar);
        this.redbar.position = this.redbar.position.SetZ(Layers.RedHealthBar);
        this.redbar.localScale = new Vector3(1, 1, 1);
    }

    void Update() {
        var renderer = GetComponent<SpriteRenderer>();
        this.greenbar.GetComponent<SpriteRenderer>().SetAlpha(renderer.color.a);
        this.redbar.GetComponent<SpriteRenderer>().SetAlpha(renderer.color.a);

        this.healthbar.position = this.transform.position.SetXY(this.transform.position + this.offset);
        this.greenbar.localScale = new Vector3(healthQuantile, 1, 1);
    }

    void OnDestroy() {
        if (this.healthbar == null) return;
        Destroy(this.healthbar.gameObject);
    }
}