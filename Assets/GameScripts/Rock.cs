﻿using UnityEngine;
using System.Collections.Generic;

class Rock : MonoBehaviour {

    public int nation { get { return team.nation; } }
    public Team team;
    private Sprite sprite;

    public float health;
    public float initialHealth;

    private GameObject trail;


    // Use this for initialization
    void Awake () {
        this.gameObject.AddOrGetComponent<PolygonCollider2D>();

        sprite = (this.renderer as SpriteRenderer).sprite;
        float area = Main.spriteToAreaRatio[sprite] * sprite.bounds.size.x * sprite.bounds.size.y;
        this.rigidbody2D.mass = area * this.transform.localScale.x;
        this.health = this.rigidbody2D.mass;
        this.initialHealth = this.health;

        //this.gameObject.AddComponent<HealthbarHost>();
        this.gameObject.AddComponent<Gravity>();

        //Try custom time trail
        this.gameObject.AddComponent<TimeTrail>();
        this.gameObject.GetComponent<TimeTrail>().emit = true;
        float[] widths = {4f,01f};
        this.gameObject.GetComponent<TimeTrail>().sizes = widths;
        Color[] colors = { new Color(.7f, .7f, .7f, .13f), new Color(.7f, .7f, .7f, .0f) };
        this.gameObject.GetComponent<TimeTrail>().lifeTime = 2f;
        var trailMat = Assets.GetMaterial("WeaponTrail");
        this.gameObject.GetComponent<TimeTrail>().material = trailMat;
        this.gameObject.GetComponent<TimeTrail>().colors = colors;
        this.gameObject.GetComponent<TimeTrail>().autoDestruct = true;
        this.gameObject.GetComponent<TimeTrail>().movePixelsForRebuild = 100; //TODO this might kill CPU
        this.gameObject.GetComponent<TimeTrail>().uvLengthScale = 2f;  //.01

        this.rigidbody2D.isKinematic = false;
        this.gameObject.tag = "Rock";
    }

    void Start() {
        this.gameObject.layer = LayerMask.NameToLayer("RocksTeam" + team.teamNumber.ToString());
    }
    
    // Update is called once per frame
    void Update () {
		////this.gameObject.renderer.material.shader = Shader.Find ("Custom/shader2");
        //this.gameObject.GetComponent<HealthbarHost>().healthQuantile = this.health / this.initialHealth;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Block" || collision.gameObject.tag == "Treasure") {
            Die();
        }
        else if (collision.gameObject.tag == "Rock") {
            Rock other = collision.gameObject.GetComponent<Rock>();
            if (other == null) return;
            if (this.team == other.team) {
                this.health -= other.AttackDamage * Constants.FRIENDLY_FIRE_DAMAGE_SCALE;
            }
            else {
                this.health -= other.AttackDamage;
            }

            if (this.health < 0) {
                if (this.team != other.team) {
                    Scoreboard.AddScore(other.team, Constants.ROCK_REWARD);

                    var scorePopup = Lifecycle.Create("ScorePopup");
                    scorePopup.transform.position = this.transform.position;
                    scorePopup.GetComponent<ScorePopup>().Initialize(Constants.ROCK_REWARD, other.team.color);
                    Lifecycle.Release(scorePopup, Constants.SCORE_POPUP_DURATION);
                }
                Die();
            }
        }
    }

    private void Die() {
        team.shotsLeft++;
        var explode = this.gameObject.AddComponent<Explode>();
        explode.ExplodeNow(sprite);
        Destroy(this.gameObject, .05f);
        Destroy(this);
    }

    public float AttackDamage {
        get { return this.rigidbody2D.mass * this.rigidbody2D.velocity.magnitude; }
    }

    void OnBuildPhaseStart () {
        Destroy(this.gameObject);
    }
}
