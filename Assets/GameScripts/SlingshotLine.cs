using UnityEngine;

class SlingshotLine : MonoBehaviour {

    public Vector2 startPoint;
    public Vector2 endPoint;

    private LineRenderer lineRenderer;

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
        Vector3 startPoint3 = this.startPoint;
        Vector3 endPoint3 = this.endPoint;

        stream.Serialize(ref startPoint3);
        stream.Serialize(ref endPoint3);

        this.startPoint = startPoint3;
        this.endPoint = endPoint3;
    }

    void Start() {
        this.lineRenderer = GetComponent<LineRenderer>();
        this.lineRenderer.SetWidth(.3f, .3f);
        this.lineRenderer.SetVertexCount(2);
    }

    void Update() {
        this.lineRenderer.SetPosition(0, this.startPoint);
        this.lineRenderer.SetPosition(1, this.endPoint);
    }
}