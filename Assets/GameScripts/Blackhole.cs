﻿using UnityEngine;
using System.Collections;

class Blackhole : MonoBehaviour {
    private const float MIN_TIME = 2f;
    private const float MAX_TIME = 30f;
    private const float MIN_VALUE = 2f;
    private const float MAX_VALUE = 30f;

    public static NetworkedSprite spaceNetworkSprite;
    public static Material spaceMaterial;
    
    public bool isRepelling;
    public int blackholeId;

    private Color color;

    private float interpStartTime;
    private float interpStartValue = 0;
    private float interpFinishTime = 0;
    private float interpFinishValue;

    // Use this for initialization
    void Start () {
        isRepelling = Random.Range(0, 2) == 0;

        this.color = isRepelling ? Constants.BLACKHOLE_REPEL_COLOR : Constants.BLACKHOLE_ATTRACT_COLOR;
        spaceMaterial.SetVector("_Color" + blackholeId.ToString(), this.color);
        spaceNetworkSprite.SetVectorProperty("_Color" + blackholeId.ToString(), this.color);

        Vector2 empircalScalar = new Vector2(0.00193f, 0.004216f);
        Vector4 position = new Vector2(this.transform.position.x * empircalScalar.x, this.transform.position.y * empircalScalar.y);
        spaceMaterial.SetVector("_Position" + blackholeId.ToString(), position);
        spaceNetworkSprite.SetVectorProperty("_Position" + blackholeId.ToString(), position);

        StartCoroutine(FadeIn());
    }

    void Update() {
        if (Time.time > interpFinishTime) {
            interpStartTime = Time.time;
            interpStartValue = interpFinishValue;
            interpFinishTime = Time.time + Random.Range(MIN_TIME, MAX_TIME);
            interpFinishValue = Random.Range(MIN_VALUE, MAX_VALUE);
        }

        float time = Mathf.Lerp(interpStartValue, interpFinishValue, (Time.time - interpStartTime) / (interpFinishTime - interpStartTime));
        spaceMaterial.SetFloat("_TimeElapsed" + blackholeId.ToString(), time);
        Blackhole.spaceNetworkSprite.SetFloatProperty("_TimeElapsed" + blackholeId.ToString(), time);
    }

    void OnBuildPhaseStart()
    {
        Destroy(this.gameObject);
    }

    void OnAttackPhaseEnd() {
        interpStartValue = Mathf.Lerp(interpStartValue, interpFinishValue, (Time.time - interpStartTime) / (interpFinishTime - interpStartTime));
        interpStartTime = Time.time;
        interpFinishTime = Time.time + Constants.ATTACK_PHASE_DESTRUCT_DURATION;
        interpFinishValue = 0;
        
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeIn() {
        float stopTime = Time.time + Constants.BUILD_PHASE_INIT_DURATION;
        while (Time.time < stopTime) {
            Vector4 color = new Vector4();
            for (int i = 0; i < 4; i++) {
                color[i] = Mathf.Lerp(this.color[i], 0, (stopTime - Time.time) / Constants.BUILD_PHASE_INIT_DURATION);
            }

            spaceMaterial.SetVector("_Color" + blackholeId.ToString(), color);
            spaceNetworkSprite.SetVectorProperty("_Color" + blackholeId.ToString(), color);
            yield return null;
        }
    }

    private IEnumerator FadeOut() {
        float stopTime = Time.time + Constants.ATTACK_PHASE_DESTRUCT_DURATION;
        while (Time.time < stopTime) {
            Vector4 color = new Vector4();
            for (int i = 0; i < 4; i++) {
                color[i] = Mathf.Lerp(0, this.color[i], (stopTime - Time.time) / Constants.ATTACK_PHASE_DESTRUCT_DURATION);
            }

            spaceMaterial.SetVector("_Color" + blackholeId.ToString(), color);
            spaceNetworkSprite.SetVectorProperty("_Color" + blackholeId.ToString(), color);
            yield return null;
        }
    }

    public static void ResetHoles() {
        spaceMaterial.SetVector("_Position0", new Vector4(99, 99, 0, 0));
        spaceNetworkSprite.SetVectorProperty("_Position0", new Vector4(99, 99, 0, 0));

        spaceMaterial.SetVector("_Position1", new Vector4(99, 99, 0, 0));
        spaceNetworkSprite.SetVectorProperty("_Position1", new Vector4(99, 99, 0, 0));

        spaceMaterial.SetVector("_Position2", new Vector4(99, 99, 0, 0));
        spaceNetworkSprite.SetVectorProperty("_Position2", new Vector4(99, 99, 0, 0));

        spaceMaterial.SetVector("_Position3", new Vector4(99, 99, 0, 0));
        spaceNetworkSprite.SetVectorProperty("_Position3", new Vector4(99, 99, 0, 0));

        spaceMaterial.SetVector("_Position4", new Vector4(99, 99, 0, 0));
        spaceNetworkSprite.SetVectorProperty("_Position4", new Vector4(99, 99, 0, 0));
    }
}
