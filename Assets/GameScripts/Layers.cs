class Layers {
    public const float Camera = -500;

    // Top layer
    public const float TitleScreen = -6;
    public const float RotationHandle = -5;
    public const float Text = -4;
    public const float LongPressIcon = -3.5f;
    public const float GreenHealthBar = -3;
    public const float RedHealthBar = -2;
    public const float BlockCounter = -1;
    public const float Block = 0;
    public const float UnspawnedBlock = 1;
    public const float RotationHandleStatic = 1.1f;
    public const float Shadow = 1.5f;
    public const float Rock0 = 2;
    public const float Rock1 = 3;
    public const float Rock2 = 4;
    public const float PlanetCollider = 4.1f;
    public const float Walls = 4.9f;
    public const float Planets = 5;
    public const float Space = 6;
    // Bottom Layer
}