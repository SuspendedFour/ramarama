using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

class NetworkedSpriteSync : MonoBehaviour {

    public int spriteID;
    public int materialID;
    public Color spriteColor;
    public bool isActive;
    public Dictionary<string, float> floatProperties = new Dictionary<string, float>();
    public Dictionary<string, Vector4> vectorProperties = new Dictionary<string, Vector4>();

    void Awake() {
        for (int i = 0; i < Constants.NUM_BLACK_HOLES_MAX; i++) {
            vectorProperties["_Color" + i.ToString()] = Vector4.zero;
            vectorProperties["_Position" + i.ToString()] = Vector4.zero;
            floatProperties["_TimeElapsed" + i.ToString()] = 0;
        }

        floatProperties["_Angle"] = 0;
        floatProperties["_Seed"] = 0;
    }

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
        Vector3 position = this.transform.position;
        Quaternion rotation = this.transform.rotation;
        Vector3 localScale = this.transform.localScale;

        Quaternion color = Vector4ToQuat(this.spriteColor);

        stream.Serialize(ref position);
        stream.Serialize(ref rotation);
        stream.Serialize(ref localScale);

        stream.Serialize(ref spriteID);
        stream.Serialize(ref materialID);
        stream.Serialize(ref color);
        stream.Serialize(ref isActive);

        if (stream.isReading) {
            this.transform.position = position;
            this.transform.rotation = rotation;
            this.transform.localScale = localScale;

            this.GetComponent<SpriteRenderer>().color = QuatToVector4(color);
            this.GetComponent<SpriteRenderer>().sprite = Assets.idToSprite[spriteID];
            this.renderer.material = Assets.idToMaterial[materialID];
            this.renderer.enabled = isActive;
        }

        foreach (var pair in floatProperties.OrderBy((kvp) => kvp.Key)) {
            float val = pair.Value;
            stream.Serialize(ref val);
            if (stream.isReading) {
                this.renderer.material.SetFloat(pair.Key, val);
            }
        }

        foreach (var pair in vectorProperties.OrderBy((kvp) => kvp.Key)) {
            Quaternion val = Vector4ToQuat(pair.Value);
            stream.Serialize(ref val);
            if (stream.isReading) {
                this.renderer.material.SetVector(pair.Key, QuatToVector4(val));
            }
        }
    }

    private static Quaternion Vector4ToQuat(Vector4 vector) {
        return new Quaternion(vector.x, vector.y, vector.z, vector.w);
    }

    private static Color QuatToVector4(Quaternion quat) {
        return new Vector4(quat.x, quat.y, quat.z, quat.w);
    }
}