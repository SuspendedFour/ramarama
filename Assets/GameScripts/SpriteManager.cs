using UnityEngine;
using System.Collections.Generic;

class SpriteManager : MonoBehaviour {
    public static Sprite GreenSquare { get; private set; }
    public static Sprite RedSquare { get; private set; }

    void Start() {
        GreenSquare = Resources.Load<Sprite>("green");
        RedSquare = Resources.Load<Sprite>("red");
    }
}