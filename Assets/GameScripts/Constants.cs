using UnityEngine;

class Constants {

    static Constants() {
        // Override with debug values
        
        // BUILD_PHASE_INIT_DURATION = 1;
        // ATTACK_PHASE_DESTRUCT_DURATION = 1;
        // CONTROLLER_ADJUSTED_VELOCITY = .42f;
        // BLOCK_MASS_TO_HEALTH_FACTOR = 5;
        // TREASURE_MASS_TO_HEALTH_FACTOR = 5;
        NUM_BLACK_HOLES_MIN = 5;  //inclusive
        // TREASURE_MIN_COUNT = 3;
        TITLE_TIME = .25f;
        SPONSORS_TIME = .25f;

        BUILD_PHASE_INIT_DURATION = 1;
    }

    //Touch filter
    public const float TOUCH_EXPIRATION = .5f;
    public const float TOUCH_DISTANCE = 10f;

    //Long press
    public const float TOUCH_THRESHOLD_DISTANCE = 5f; // How far a touch needs to go before we're sure it's not a long press
    public const float GHOST_BLOCK_LONGPRESS_DISTANCE = 2f;
    public const float LONGPRESS_DURATION = 1f;


    //Timers (seconds)
    static public int BUILD_TIME = 330;
    static public int ATTACK_TIME = 300;
    static public int END_GAME_TIME = 30; //viewing stats? small break between games?
    static public int BUILD_PHASE_INIT_DURATION = 5; // Fade everything in, controls disabled
    static public int ATTACK_PHASE_INIT_DURATION = 4; // Only affects text and block fade
    static public int ATTACK_PHASE_DESTRUCT_DURATION = 5;
    static public float TITLE_TIME = 10;
    static public float SPONSORS_TIME = 5;

    //Treasure limits
    static public int TREASURE_MIN_COUNT = 1;
    static public int TREASURE_MAX_COUNT = 5;
    static public float TREASURE_MARGINS = 12;
    static public float TREASURE_MAX_HEIGHT = Tiles.tileWidth*.7f;
    public const float SHINE_FPS = 20;

    //block limits
    static public int BLOCKS_PER_TEAM_MIN = 40;  //inclusive
    static public int BLOCKS_PER_TEAM_MAX = 64;  //inclusive
    static public float BLOCK_MOVE_DIST_REQUIRED = 3f; // Drag distance required to spawn a block

    //controller limits
    static public float CONTROLLER_MAX_STRETCH = 10f;
    static public float CONTROLLER_ADJUSTED_VELOCITY = .02f;
    static public int MAX_SHOTS = 2;

    //Gravity, blackholes spawns
    static public float PLANET_GRAVITY_CONST = 7f;      // how strong is planet gravity's acceleration?
    static public float PLANET_GRAV_EXPONENT = 1.5f;     // how much stronger is gravity very near the planet?
    static public float PLANET_GRAV_DEADZONE = 30;       // how large is the "planet gravity deadzone" in the middle?
    static public float BLACKHOLE_GRAVITY_CONST = 80.0f; // how strong is blackhole gravity's acceleration?
    static public float BLACKHOLE_GRAV_EXPONENT = 1.25f; // how much stronger is gravity very near the blackhole?
    static public float BLACKHOLE_MASS = 1; //temp crappy varible that needs to be dynamically assigned to blackhole size TODO
    static public int NUM_BLACK_HOLES_MIN = 0;  //inclusive
    static public int NUM_BLACK_HOLES_MAX = 5;  //inclusive (increasing this number beyond 5 requires modifying the shader)

    static public Vector4 BLACKHOLE_ATTRACT_COLOR = new Vector4(.2f, 0, 0, 0);
    static public Vector4 BLACKHOLE_REPEL_COLOR = new Vector4(.1f, .1f, 0, 0);

    //Wall physics

    //Health ratios
    static public float BLOCK_MASS_TO_HEALTH_FACTOR = 20;
    static public float FRIENDLY_FIRE_DAMAGE_SCALE = .1f;
    static public float TREASURE_MASS_TO_HEALTH_FACTOR = 20;

    //Score system
    static public int BLOCK_REWARD = 10;
    static public int ROCK_REWARD = 1;
    static public int TREASURE_REWARD = 100;

    static public float HEALTHBAR_WIDTH = 10;
    static public float HEALTHBAR_HEIGHT = 1;
    static public float HEALTHBAR_OFFSET = 3;

    static public float SCORE_POPUP_SCALE = 5;
    static public float SCORE_POPUP_DURATION = 1;

    // Visual scale
    static public float BLOCK_SIZE = Tiles.tileWidth / 20f;
    static public float ROCK_SIZE = BLOCK_SIZE;
    static public float TREASURE_SIZE = BLOCK_SIZE * 1.5f;

    //INET
    public const int port = 12344;

}