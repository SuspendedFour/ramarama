﻿using UnityEngine;
using System.Collections;

public class RotationController {

    private bool inputInitialized;
    private Angle initialInput;
    private Angle previousInput;
    private int halfRevolution;
    private bool fullCircleComplete;

    // These are relative to StartRotation()
    public bool IsReflected { get; private set; }
    public Angle Z { get; private set; }

    public void StartRotation() {
        halfRevolution = 0;
        fullCircleComplete = false;
        inputInitialized = false;
        Z = 0;
        IsReflected = false;
    }

    public void ProcessInput(Angle input) {
        if (!inputInitialized) {
            initialInput = input;
            previousInput = input;
            inputInitialized = true;
            return;
        }

        if (fullCircleComplete) {
            if (Pass360(previousInput, input)) {
                IsReflected = !IsReflected;
            }
        }
        else {
            if (Pass180Clockwise(previousInput, input)) {
                halfRevolution++;
            }
            else if (Pass180Counter(previousInput, input)) {
                halfRevolution--;
            }
            else if (Pass360(previousInput, input) && halfRevolution != 0) {
                fullCircleComplete = true;
                IsReflected = !IsReflected;
            }
        }

        this.previousInput = input;
        this.Z = input - initialInput;
    }

    private bool Pass180Clockwise(Angle previousInput, Angle newInput) {
        Angle semiCircle = initialInput + 180;
        if (!semiCircle.InBetween(previousInput, newInput)) return false;

        return previousInput < newInput;
    }

    private bool Pass180Counter(Angle previousInput, Angle newInput) {
        Angle semiCircle = initialInput + 180;
        if (!semiCircle.InBetween(previousInput, newInput)) return false;

        return newInput < previousInput;
    }

    private bool Pass360(Angle previousInput, Angle newInput) {
        return initialInput.InBetween(previousInput, newInput);
    }
}