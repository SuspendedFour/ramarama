using UnityEngine;

class ShotsLeft : MonoBehaviour {
    public Team team;

    private NumberString count;
    private SpriteRenderer colonRenderer;
    private SpriteRenderer shotsLeftRenderer;

    void Start() {
        count = this.transform.Find("Count").GetComponent<NumberString>();
        colonRenderer = this.transform.Find("Colon").GetComponent<SpriteRenderer>();
        shotsLeftRenderer = this.GetComponent<SpriteRenderer>();

        count.AllocateDigits(3);
        count.Hide();
    }

    void Update() {
        count.number = team.shotsLeft;
    }

    public void OnBuildPhaseEnd() {
        count.color = team.color;
        colonRenderer.color = team.color;
        shotsLeftRenderer.color = team.color;

        count.BeginFade(Constants.ATTACK_PHASE_INIT_DURATION, 0, 1);
        colonRenderer.GetComponent<Fade>().FadeIn(Constants.ATTACK_PHASE_INIT_DURATION);
        shotsLeftRenderer.GetComponent<Fade>().FadeIn(Constants.ATTACK_PHASE_INIT_DURATION);
    }

    public void OnAttackPhaseEnd() {
        count.BeginFade(Constants.ATTACK_PHASE_DESTRUCT_DURATION, 1, 0);
        colonRenderer.GetComponent<Fade>().FadeOut(Constants.ATTACK_PHASE_DESTRUCT_DURATION);
        shotsLeftRenderer.GetComponent<Fade>().FadeOut(Constants.ATTACK_PHASE_DESTRUCT_DURATION);
    }
}