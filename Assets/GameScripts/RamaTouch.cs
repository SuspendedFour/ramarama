﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RamaTouch {
    private const int MOUSE_TOUCH_ID = 9999999;

    public Vector2 position { get; private set; }
    public TouchPhase phase { get; private set; }
    public int touchId { get; private set; }

    private static int touchCounter = 0;
    private static Dictionary<int, int> tuioTouchToRamaTouch = new Dictionary<int, int>();
    private static List<DyingTouch> dyingTouches = new List<DyingTouch>();

    public RamaTouch(Vector2 position, TouchPhase phase, int touchId) {
        this.position = position;
        this.phase = phase;
        this.touchId = touchId;
    }

    private RamaTouch() { }


    // This is the non filtered implementation

    // public static RamaTouch FromTouch(Touch touch) {
    //     RamaTouch result = new RamaTouch();
    //     result.position = touch.position;
    //     result.phase = touch.phase;

    //     if (tuioTouchToRamaTouch.ContainsKey(touch.fingerId)) {
    //         result.touchId = tuioTouchToRamaTouch[touch.fingerId];
    //     }
    //     else {
    //         result.touchId = touch.fingerId;
    //     }

    //     return result;
    // }


    public static RamaTouch FromTouch(Touch touch) {
        if (touch.phase == TouchPhase.Began) {
            return TouchDown(touch);
        }

        if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
            TouchUp(touch);
        }

        RamaTouch result = new RamaTouch();
        result.position = touch.position;
        result.phase = touch.phase;
        result.touchId = tuioTouchToRamaTouch[touch.fingerId];

        return result;
    }

    private static RamaTouch TouchDown(Touch touch) {
        dyingTouches.RemoveAll((dyingTouch) => Time.time > dyingTouch.time + Constants.TOUCH_EXPIRATION);

        DyingTouch closestDyingTouch = null;
        foreach (var dyingTouch in dyingTouches) {
            if (closestDyingTouch == null) {
                closestDyingTouch = dyingTouch;
            }
            else if (Vector2.Distance(touch.position, dyingTouch.position) < Vector2.Distance(touch.position, closestDyingTouch.position)) {
                closestDyingTouch = dyingTouch;
            }
        }

        if (closestDyingTouch != null && Vector2.Distance(touch.position, closestDyingTouch.position) < Constants.TOUCH_DISTANCE) {
            tuioTouchToRamaTouch[touch.fingerId] = closestDyingTouch.ramaID;
            dyingTouches.Remove(closestDyingTouch);
        }
        else {
            tuioTouchToRamaTouch[touch.fingerId] = AllocateTouchID();
        }

        RamaTouch result = new RamaTouch();
        result.position = touch.position;
        result.phase = touch.phase;
        result.touchId = tuioTouchToRamaTouch[touch.fingerId];
        return result;
    }

    private static void TouchUp(Touch touch) {
        dyingTouches.Add(new DyingTouch(touch.position, Time.time, tuioTouchToRamaTouch[touch.fingerId]));
    }

    public static RamaTouch FromMouse0() {
        RamaTouch mouseTouch = null;
        if (Input.GetMouseButtonDown(0)) {
            mouseTouch = new RamaTouch();
            mouseTouch.position = Input.mousePosition;
            mouseTouch.phase = TouchPhase.Began;
            mouseTouch.touchId = MOUSE_TOUCH_ID;
        }
        else if (Input.GetMouseButton(0)) {
            mouseTouch = new RamaTouch();
            mouseTouch.position = Input.mousePosition;
            mouseTouch.phase = TouchPhase.Moved;
            mouseTouch.touchId = MOUSE_TOUCH_ID;
        }
        else if (Input.GetMouseButtonUp(0)) {
            mouseTouch = new RamaTouch();
            mouseTouch.position = Input.mousePosition;
            mouseTouch.phase = TouchPhase.Ended;
            mouseTouch.touchId = MOUSE_TOUCH_ID;
        }

        return mouseTouch;
    }

    public static int AllocateTouchID() {
        return touchCounter++;
    }

    private class DyingTouch {
        public Vector2 position;
        public float time;
        public int ramaID;

        public DyingTouch(Vector2 position, float time, int ramaID) {
            this.position = position;
            this.time = time;
            this.ramaID = ramaID;
        }
    }
}
