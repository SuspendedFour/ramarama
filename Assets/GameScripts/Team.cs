using UnityEngine;

class Team {
    public static Team[] teams = new Team[2];
    public static Team leftTeam;
    public static Team rightTeam;
    static Team() {
        teams[0] = new Team();
        teams[1] = new Team();
        leftTeam = teams[0];
        rightTeam = teams[1];
    }

    public Color color;
    public NationSpriteSet spriteSet;
    public int nation;
    public int teamNumber;
    public int remainingTreasures;
    public int shotsLeft;
}