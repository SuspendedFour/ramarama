using UnityEngine;
using System;

class CapturedTouchEvent : TouchEvent {
    private Action onRelease;
    private Action<GameObject> onTransfer;

    public CapturedTouchEvent(Vector3 position, int touchId, Action onRelease, Action<GameObject> onTransfer) : base(position, touchId) {
        this.onRelease = onRelease;
        this.onTransfer = onTransfer;
    }

    public void Transfer(GameObject gameObject) {
        if (onTransfer != null) {
            onTransfer(gameObject);
        }
    }

    public void Release() {
        if (onRelease != null) {
            onRelease();
        }
    }
}