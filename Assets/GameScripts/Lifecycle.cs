using UnityEngine;
using System.Collections;

// Start using this class to instantiate/destroy objects in case we need to pool
class Lifecycle : MonoBehaviour {

    private static Lifecycle instance;

    public Lifecycle() {
        instance = this;
    }

    public static GameObject Create(string prefabPath) {
        return (GameObject) Object.Instantiate(Assets.pathToPrefab[prefabPath]);
    }

    public static void Release(GameObject gameObject) {
        Destroy(gameObject);
    }

    public static void Release(GameObject gameObject, float delaySec) {
        instance.StartCoroutine(instance.ReleaseRoutine(gameObject, delaySec));
    }

    private IEnumerator ReleaseRoutine(GameObject gameObject, float delaySec) {
        yield return new WaitForSeconds(delaySec);
        Destroy(gameObject);
    }
}