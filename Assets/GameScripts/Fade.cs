using UnityEngine;
using System.Collections;

class Fade : MonoBehaviour {

    private float startTime;
    private float duration;

    private float startAlpha;
    private float endAlpha;

    void Update() {
        if (Time.time > startTime + duration + .05f) return;

        float t = (Time.time - startTime) / duration;
        float alpha = Mathf.Lerp(startAlpha, endAlpha, t);

        var renderer = this.gameObject.GetComponent<SpriteRenderer>();
        renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);
    }

    public void BeginFade(float duration, float startAlpha, float endAlpha) {
        var renderer = GetComponent<SpriteRenderer>();
        renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, startAlpha);

        this.startTime = Time.time;
        this.duration = duration;
        this.startAlpha = startAlpha;
        this.endAlpha = endAlpha;
    }

    public void FadeIn(float delay, float duration) {
        StartCoroutine(BeginFadeRoutine(delay, duration, 0, 1));
    }

    public void FadeIn(float duration) {
        BeginFade(duration, 0, 1);
    }

    public void FadeOut(float delay, float duration) {
        StartCoroutine(BeginFadeRoutine(delay, duration, 1, 0));
    }

    public void FadeOut(float duration) {
        BeginFade(duration, 1, 0);
    }

    public void FadeOutFromCurrent(float duration) {
        BeginFade(duration, GetComponent<SpriteRenderer>().color.a, 0);
    }

    private IEnumerator BeginFadeRoutine(float delay, float duration, float startAlpha, float endAlpha) {
        yield return new WaitForSeconds(delay);
        BeginFade(duration, startAlpha, endAlpha);
    }
}