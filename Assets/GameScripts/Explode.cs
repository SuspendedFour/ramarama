﻿using UnityEngine;
using System.Collections;

public class Explode : MonoBehaviour {

    private Color AverageColorFromTexture(Texture2D tex) {
        Color32[] texColors = tex.GetPixels32();
        int total = texColors.Length;
        float r = 0;
        float g = 0;
        float b = 0;
        float a = 0;
        for (int i = 0; i < total; i++) {
            r += texColors[i].r;
            g += texColors[i].g;
            b += texColors[i].b;
            a += texColors[i].a;
        }
        return new Color((r / total / 255f), (g / total / 255f), (b / total / 255f), .8f);// (a / total / 255));
    }

    public void ExplodeNow(Sprite sprite) {
        Color startColor = AverageColorFromTexture(sprite.texture);
        Vector3 startColorRgb = new Vector3(startColor.r, startColor.g, startColor.b);
        NetworkMain.instance.RPC("CreateExplosion", RPCMode.All, this.transform.position, startColorRgb, startColor.a);
    }
}
