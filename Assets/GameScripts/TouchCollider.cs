using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Should only have one instance
class TouchCollider : MonoBehaviour {
    Dictionary<int, GameObject> touchidToGameObject = new Dictionary<int, GameObject>();

    // PERFORMANCE: Are these shortlived callbacks causing GC hell?
    void Update() {
        foreach (RamaTouch touchTemp in RamaInput.Touches) {
            // Create closure variable
            RamaTouch touch = touchTemp;

            Vector3 wp = Utils.ScreenPointToWorld(touch.position);
            Vector2 touchPos = new Vector2(wp.x, wp.y);

            // Prevent non-existent callbacks
            if (touchidToGameObject.ContainsKey(touch.touchId)) {
                if (touchidToGameObject[touch.touchId] == null) {
                    touchidToGameObject.Remove(touch.touchId);
                }
            }

            if (touch.phase == TouchPhase.Began) {
                Collider2D collider = Physics2D.OverlapPoint(touchPos);
                if (collider == null) return;

                Action<GameObject> onCapture = (gameObject) => touchidToGameObject[touch.touchId] = gameObject;
                TouchEvent touchEvent = new UncapturedTouchEvent(touchPos, touch.touchId, onCapture);
                collider.gameObject.SendMessage("OnTouchDown", touchEvent, SendMessageOptions.DontRequireReceiver);
            }
            else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) {

                if (touchidToGameObject.ContainsKey(touch.touchId)) {
                    // At this point, an object has captured the touch and user is dragging object

                    Action onRelease = () => touchidToGameObject.Remove(touch.touchId);

                    // This allows objects to transfer control to another object
                    Action<GameObject> onTransfer = (gameObject) => { touchidToGameObject[touch.touchId] = gameObject; };

                    TouchEvent touchEvent = new CapturedTouchEvent(touchPos, touch.touchId, onRelease, onTransfer);
                    touchidToGameObject[touch.touchId].gameObject.SendMessage("OnTouchDrag", touchEvent, SendMessageOptions.DontRequireReceiver);
                }
                else {
                    // This is the case where we're hovering over an object but this touch hasn't been captured

                    Collider2D collider = Physics2D.OverlapPoint(touchPos);
                    if (collider == null) return;

                    Action<GameObject> onCapture = (gameObject) => touchidToGameObject[touch.touchId] = gameObject;
                    TouchEvent touchEvent = new UncapturedTouchEvent(touchPos, touch.touchId, onCapture);
                    collider.gameObject.SendMessage("OnTouchOver", touchEvent, SendMessageOptions.DontRequireReceiver);
                }
            }
            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
                if (touchidToGameObject.ContainsKey(touch.touchId)) {
                    TouchEvent touchEvent = new CapturedTouchEvent(touchPos, touch.touchId, null, null);
                    touchidToGameObject[touch.touchId].SendMessage("OnTouchUp", touchEvent, SendMessageOptions.DontRequireReceiver);
                    touchidToGameObject.Remove(touch.touchId);
                }
            }
        }
    }
}