﻿using UnityEngine;
using System.Collections;

public class SexyTimer : MonoBehaviour {
    public const float FADE_TIME = .5f;

    GameObject mainClock;
    GameObject tenMinute;
    GameObject oneMinute;
    GameObject colon;
    GameObject tenSecond;
    GameObject oneSecond;

    public Color color {
        set {
           oneMinute.gameObject.GetComponent<SpriteRenderer>().color = value;
           colon.gameObject.GetComponent<SpriteRenderer>().color = value;
           tenSecond.gameObject.GetComponent<SpriteRenderer>().color = value;
           oneSecond.gameObject.GetComponent<SpriteRenderer>().color = value;
        }
    }

    public void SetTime(string time) {
        //tenMinute.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/" + time.Substring(0, 1) + "_outline");
        oneMinute.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/" + time.Substring(1, 1) + "_outline");
        tenSecond.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/" + time.Substring(3, 1) + "_outline");
        oneSecond.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/" + time.Substring(4, 1) + "_outline");
    }

    void Awake() {
        mainClock = this.gameObject;

        //tenMinute = Instantiate(Resources.Load("Prefabs/Number")) as GameObject;
        oneMinute = Instantiate(Resources.Load("Prefabs/Number")) as GameObject;
        colon = Instantiate(Resources.Load("Prefabs/Number")) as GameObject;
        tenSecond = Instantiate(Resources.Load("Prefabs/Number")) as GameObject;
        oneSecond = Instantiate(Resources.Load("Prefabs/Number")) as GameObject;

        //tenMinute.transform.parent = mainClock.transform;
        oneMinute.transform.parent = mainClock.transform;
        colon.transform.parent = mainClock.transform;
        tenSecond.transform.parent = mainClock.transform;
        oneSecond.transform.parent = mainClock.transform;


        //tenMinute.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/1_outline");
        oneMinute.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/2_outline");
        colon.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/Chars/colonoscopy_outline");
        tenSecond.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/3_outline");
        oneSecond.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Numbers/4_outline");

        //tenMinute.gameObject.transform.localPosition = new Vector2(-12f,0);
        oneMinute.gameObject.transform.localPosition = new Vector2(-5f, 0);
        colon.gameObject.transform.localPosition = new Vector2(0, 0);
        tenSecond.gameObject.transform.localPosition = new Vector2(5f, 0);
        oneSecond.gameObject.transform.localPosition = new Vector2(12f, 0);
    }

    public void BeginFade(float duration, float startAlpha, float endAlpha) {
        oneMinute.GetComponent<Fade>().BeginFade(duration, startAlpha, endAlpha);
        colon.GetComponent<Fade>().BeginFade(duration, startAlpha, endAlpha);
        tenSecond.GetComponent<Fade>().BeginFade(duration, startAlpha, endAlpha);
        oneSecond.GetComponent<Fade>().BeginFade(duration, startAlpha, endAlpha);
    }
}
