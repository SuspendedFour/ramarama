using UnityEngine;

class ScorePopup : MonoBehaviour {
    void Start() {
        transform.localScale = new Vector3(Constants.SCORE_POPUP_SCALE, Constants.SCORE_POPUP_SCALE, 1);
        Tiles.OrientUpward(transform);
    }

    public void Initialize(int score, Color color) {
        var numberString = GetComponent<NumberString>();
        numberString.AllocateDigits(score.ToString().Length + 1);
        numberString.FadeOut(Constants.SCORE_POPUP_DURATION);
        numberString.number = score;
        numberString.color = color;
    }
}