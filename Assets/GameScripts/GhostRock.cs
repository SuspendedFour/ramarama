using UnityEngine;
using System.Collections.Generic;

class GhostRock : MonoBehaviour {
    public Team team;
    public Vector2 initialTouch;

    private GameObject line;

    // Used to detect release in case OnTouchUp is never received
    private int onDragCounter = 0;

    void Awake() {
        this.transform.position = this.transform.position.SetZ(Layers.Rock0);
    }

    void Update() {
        onDragCounter--;
        if (onDragCounter < -2) {
            OnTouchUp();
        }
    }

    public void Initialize(Team team, Vector2 initialTouch) {
        this.team = team;
        this.initialTouch = initialTouch;

        var rock0s = team.spriteSet.rockLayers[0];
        var rock1s = team.spriteSet.rockLayers[1];
        var rock2s = team.spriteSet.rockLayers[2];
        var standaloneRocks = team.spriteSet.standaloneRocks;

        int totalRockSprites = rock0s.Count + rock1s.Count + rock2s.Count + standaloneRocks.Count;
        int rando = Random.Range(0, totalRockSprites);

        if (rando < standaloneRocks.Count) {
            (this.renderer as SpriteRenderer).sprite = standaloneRocks[rando];
        }
        else {
            (this.renderer as SpriteRenderer).sprite = rock0s[Random.Range(0, rock0s.Count)];
            InitializeLayers();
        }

        line = Network.Instantiate(Assets.pathToPrefab["SlingshotLine"], Vector3.zero, Quaternion.identity, 0) as GameObject;
        line.GetComponent<SlingshotLine>().startPoint = initialTouch;
        line.GetComponent<SlingshotLine>().endPoint = initialTouch;
    }

    private void InitializeLayers() {
        var rock1s = team.spriteSet.rockLayers[1];
        var rock2s = team.spriteSet.rockLayers[2];
        
        GameObject layer1 = new GameObject();
        GameObject layer2 = new GameObject();

        layer1.transform.parent = this.transform;
        layer2.transform.parent = this.transform;

        layer1.transform.localPosition = new Vector3();
        layer2.transform.localPosition = new Vector3();

        layer1.transform.localScale = new Vector3(1, 1, 1);
        layer2.transform.localScale = new Vector3(1, 1, 1);

        float myZ = this.transform.position.z;
        layer1.transform.localPosition = layer1.transform.localPosition.SetZ(Layers.Rock1 - myZ);
        layer2.transform.localPosition = layer2.transform.localPosition.SetZ(Layers.Rock2 - myZ);

        var renderer1 = layer1.gameObject.AddComponent<SpriteRenderer>();
        var renderer2 = layer2.gameObject.AddComponent<SpriteRenderer>();
        renderer1.sprite = rock1s[Random.Range(0, rock1s.Count)];
        renderer2.sprite = rock2s[Random.Range(0, rock2s.Count)];
        layer1.gameObject.AddComponent<NetworkedSprite>();
        layer2.gameObject.AddComponent<NetworkedSprite>();

        layer1.AddComponent<FadeOutOnEndGame>();
        layer2.AddComponent<FadeOutOnEndGame>();
    }

    void OnTouchDrag(CapturedTouchEvent touch) {
        Vector2 touchPos = touch.position-initialTouch;
        float touchLength = touchPos.magnitude;
        if (touchLength > Constants.CONTROLLER_MAX_STRETCH) {
            touchPos.Normalize();
            touchPos *= Constants.CONTROLLER_MAX_STRETCH;
        }

        line.GetComponent<SlingshotLine>().endPoint = touchPos + initialTouch;
        this.transform.position = this.transform.position.SetXY(touchPos + initialTouch);
        onDragCounter++;
    }

    void OnTouchUp() {
        Destroy(this.gameObject.GetComponent<GhostRock>());
        var rockScript = this.gameObject.AddComponent<Rock>();
        rockScript.team = this.team;
        this.rigidbody2D.velocity = Vector2.Scale(initialTouch - (Vector2)this.transform.position,
            new Vector2(Mathf.Abs(this.transform.position.x)*Constants.CONTROLLER_ADJUSTED_VELOCITY,
                Mathf.Abs(this.transform.position.x) * Constants.CONTROLLER_ADJUSTED_VELOCITY));
        Network.Destroy(this.line.GetComponent<NetworkView>().viewID);
    }
}