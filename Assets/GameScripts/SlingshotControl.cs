using UnityEngine;

class SlingshotControl : MonoBehaviour {

    private static Object rockPrefab;

    void Start() {
        if (rockPrefab == null) {
            rockPrefab = Resources.Load("Prefabs/Rock");
        }
    }

    void OnLongPress(LongPressEventData eventData) {
        Vector2 initialTouch = eventData.initialTouch;
        CapturedTouchEvent lastTouch = eventData.lastTouch;

        Team team;
        if (Tiles.LeftMost.Contains(initialTouch)) {
            team = Team.teams[0];
        }
        else {
            team = Team.teams[1];
        }

        // TODO: Determine when long press begins, and indicate to player
        if (team.shotsLeft <= 0) return;
        
        team.shotsLeft--;

        var rock = (GameObject) Instantiate(rockPrefab);
        rock.transform.localPosition = rock.transform.localPosition.SetXY(initialTouch);
        rock.transform.localScale = new Vector3(Main.rockSize, Main.rockSize, 1);
        lastTouch.Transfer(rock.gameObject);

        rock.GetComponent<GhostRock>().Initialize(team, initialTouch);
    }

    void OnBuildPhaseEnd() {
        this.collider2D.enabled = true;
    }

    void OnAttackPhaseEnd() {
        this.collider2D.enabled = false;
    }
}