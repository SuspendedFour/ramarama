﻿using UnityEngine;
using System.Collections;

public class RamaTouchGUI : MonoBehaviour {

    public Texture2D texture;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI() {
        foreach (var touch in RamaInput.Touches) {
            float size = 25f;
            GUI.DrawTexture(new Rect(touch.position.x - size / 2, Screen.height - touch.position.y - size / 2, size, size), texture);
        }
    }
}
