using UnityEngine;

class FadeOutOnEndGame : MonoBehaviour {
    void Awake() {
        this.gameObject.AddOrGetComponent<Fade>();
    }

    void OnAttackPhaseEnd() {
        float duration = .5f;
        float phase = Random.Range(0, Constants.ATTACK_PHASE_DESTRUCT_DURATION - duration);
        this.gameObject.GetComponent<Fade>().FadeOut(phase, duration);
    }
}