using UnityEngine;
using System.Collections;

public class CrossFade : MonoBehaviour {

    private Sprite oldSprite;
    private GameObject oldSpriteObj;
    private bool fadeInProgress = false;

    void Awake() {
        this.gameObject.AddOrGetComponent<Fade>();
    }

    public void BeginFade(Sprite finalSprite, float duration) {
        var renderer = this.gameObject.GetComponent<SpriteRenderer>();
        if (renderer.sprite == finalSprite) return;

        this.oldSprite = renderer.sprite;
        renderer.sprite = finalSprite;

        if (finalSprite != null) {
            this.GetComponent<Fade>().FadeIn(duration);
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0);
        }

        if (this.oldSprite != null) {
            this.oldSpriteObj = Instantiate(Assets.spritePrefab) as GameObject;
            this.oldSpriteObj.GetComponent<SpriteRenderer>().sprite = oldSprite;
            this.oldSpriteObj.transform.position = this.transform.position;
            this.oldSpriteObj.transform.rotation = this.transform.rotation;
            this.oldSpriteObj.transform.localScale = this.transform.localScale;
            
            var fade = this.oldSpriteObj.AddComponent<Fade>();
            fade.FadeOut(duration);
        }

        StartCoroutine(EndFade(duration));
        this.fadeInProgress = true;
    }

    void Update() {
        if (!this.fadeInProgress || this.oldSpriteObj == null) return;

        this.oldSpriteObj.transform.position = this.transform.position.SetZ(this.transform.position.z - .1f);
        this.oldSpriteObj.transform.rotation = this.transform.rotation;
        this.oldSpriteObj.transform.localScale = this.transform.localScale;
    }

    IEnumerator EndFade(float duration) {
        yield return new WaitForSeconds(duration);
        Destroy(this.oldSpriteObj);
        this.fadeInProgress = false;
    }

    void UpdateTransform() {
    }
}