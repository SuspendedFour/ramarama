﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class GhostBlock : MonoBehaviour {
    private Object rotationHandlePrefab;
    private GameObject rotationHandle;

    public Team team;

    private bool isDrag;
    private Vector3 mouseOffset;
    private int touchId;
    private bool controlActive;
    private Quaternion currentRotation;
    private bool justSpawned;

    // Use this for initialization
    void Awake () {
        this.gameObject.name = "GhostBlock";
        this.transform.position = this.transform.position.SetZ(Layers.Block);
        this.rotationHandlePrefab = Resources.Load("Prefabs/RotationHandle");
        this.rotationHandle = (GameObject)Instantiate(rotationHandlePrefab);
        this.rotationHandle.transform.parent = this.transform;
        this.rotationHandle.transform.localPosition = new Vector3();
        this.rotationHandle.transform.position = this.rotationHandle.transform.position.SetZ(Layers.RotationHandle);
        this.rotationHandle.transform.localScale = new Vector3(1, 1, 1);
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, .75f);
        this.gameObject.layer = LayerMask.NameToLayer("Ghost");

        this.gameObject.AddOrGetComponent<Rigidbody2D>();
        var longpress = this.gameObject.AddOrGetComponent<LongPress>();
        longpress.icon = Assets.pathToSprite["Timer/timer_drop"];
        longpress.touchThreshholdDistance = Constants.GHOST_BLOCK_LONGPRESS_DISTANCE;
    }

    public void JustSpawned(Vector3 mouseOffset) {
        this.isDrag = true;
        this.mouseOffset = mouseOffset;
        this.currentRotation = this.transform.rotation;
    }

    public void JustSpawned() {
        this.isDrag = true;
        this.justSpawned = true;
        this.currentRotation = this.transform.rotation;
    }

    void Update() {
        this.rigidbody2D.angularVelocity = 0;
        if (controlActive) {
            this.transform.rotation = currentRotation;
        }
    }

    void OnTouchDown(UncapturedTouchEvent touch) {
        touch.Capture(this.gameObject);
        mouseOffset = (Vector3) touch.position - transform.position;
        mouseOffset.z = 0;
        isDrag = false;
        currentRotation = this.transform.rotation;
    }

    void OnTouchDrag(CapturedTouchEvent touch) {
        if (justSpawned) {
            this.mouseOffset = touch.position - transform.position.XY();
            justSpawned = false;
        }

        if (isDrag) {
            controlActive = true;
            this.rigidbody2D.isKinematic = false;
            Vector2 target = touch.position - mouseOffset.XY();
            this.rigidbody2D.velocity = target - this.transform.position.XY();
            this.rigidbody2D.velocity *= 10;
        }
    }

    void OnTouchUp(CapturedTouchEvent touch) {
        this.rigidbody2D.isKinematic = true;
        this.transform.rotation = currentRotation;
        this.controlActive = false;
    }

    void OnLongPressCancelled() {
        isDrag = true;
    }

    void OnLongPress() {
        StartCoroutine("LongPressRoutine");
    }

    private IEnumerator LongPressRoutine() {
        // Wait 1 frame to prevent immediate LongPress callback in new script 
        yield return null;
        Destroy(this.rotationHandle);
        Destroy(this.gameObject.GetComponent<GhostBlock>());
        LiveBlock liveBlockScript = this.gameObject.AddComponent<LiveBlock>();
        liveBlockScript.team = this.team;
    }

    void OnBuildPhaseEnd() {
        Destroy(this.gameObject);
    }
}
