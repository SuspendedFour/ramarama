using UnityEngine;

class RamaServer : MonoBehaviour {
    private int maxConnections = 99;
    private int port = Constants.port;

    void Awake() {
        Network.InitializeServer(maxConnections, port, useNat: false);
    }
}