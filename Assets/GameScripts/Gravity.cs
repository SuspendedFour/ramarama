﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour {
    public float PLANET_GRAVITY_CONST = Constants.PLANET_GRAVITY_CONST;      // how strong is planet gravity's acceleration?
    public float PLANET_GRAV_EXPONENT = Constants.PLANET_GRAV_EXPONENT;     // how much stronger is gravity very near the planet?
    public float PLANET_GRAV_DEADZONE = Constants.PLANET_GRAV_DEADZONE;       // how large is the "planet gravity deadzone" in the middle?
    public float PLANET_CENTER = Tiles.right;             // where is the center of a planet from the center of world? 
    public float BLACKHOLE_GRAVITY_CONST = Constants.BLACKHOLE_GRAVITY_CONST; // how strong is blackhole gravity's acceleration?
    public float BLACKHOLE_GRAV_EXPONENT = Constants.BLACKHOLE_GRAV_EXPONENT; // how much stronger is gravity very near the blackhole?
    public float BLACKHOLE_MASS = Constants.BLACKHOLE_MASS; //temp crappy varible that needs to be dynamically assigned to blackhole size TODO

    private Vector2 pos = new Vector2(0,0);

    private GameObject[] holes;

    void Start()
    {
        holes = GameObject.FindGameObjectsWithTag("Blackhole");
    }

    void FixedUpdate()
    {
        pos = transform.position;
        if (pos.x < -PLANET_GRAV_DEADZONE)
        {
            rigidbody2D.AddForce(new Vector2(-doPlanetGravity(), 0));
        }
        else if (pos.x > PLANET_GRAV_DEADZONE)
        {
            rigidbody2D.AddForce(new Vector2(doPlanetGravity(), 0));
        }
        if (this.tag == "Rock")
        {
            rigidbody2D.AddForce(doBlackHoleGravity());
        }
    }

    float doPlanetGravity() {
        float distanceFromSurface = Mathf.Abs(pos.x);   //inverted here already
        //print(Mathf.Pow(distanceFromSurface / PLANET_CENTER, GRAV_EXPONENT));
        return PLANET_GRAVITY_CONST * rigidbody2D.mass *          //normal gravity
            //distanceFromSurface / affectArea;                 //linear scaling distance to surface
            Mathf.Pow(distanceFromSurface / PLANET_CENTER, PLANET_GRAV_EXPONENT); //squared scaling of gravity (more realistic)
    }

    Vector2 doBlackHoleGravity()
    {
        Vector2 affect = new Vector2(0, 0);
        foreach (GameObject hole in holes) {
            Vector2 direction = (pos - (Vector2)hole.transform.position);
            direction.Normalize();
            float distance = Vector2.Distance(pos, (Vector2)hole.transform.position);
            float gravityForceAtThisPoint = BLACKHOLE_GRAVITY_CONST * BLACKHOLE_MASS * rigidbody2D.mass / (Mathf.Pow(distance, BLACKHOLE_GRAV_EXPONENT));
            if (hole.gameObject.AddOrGetComponent<Blackhole>().isRepelling)
                affect += direction * gravityForceAtThisPoint;  //TODO repellant blackhole
            else    
                affect -= direction * gravityForceAtThisPoint;  // pull towards the well
        }
        return affect;
    }
}
