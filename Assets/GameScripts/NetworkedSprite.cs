using UnityEngine;
using System.Collections;

class NetworkedSprite : MonoBehaviour {
    private static Object networkedSpritePrefab;
    private static GameObject networkSpriteFolder;

    private GameObject networkedSprite;
    private NetworkedSpriteSync sync;

    void Awake() {
        if (networkedSpritePrefab == null) {
            networkedSpritePrefab = Resources.Load("Prefabs/NetworkedSprite");
        }

        if (networkSpriteFolder == null) {
            networkSpriteFolder = GameObject.Find("Networked Sprites");
        }

        this.networkedSprite = (GameObject) Network.Instantiate(networkedSpritePrefab, Vector3.zero, Quaternion.identity, 0);
        this.networkedSprite.name = this.name + " (Networked)";
        this.networkedSprite.transform.parent = networkSpriteFolder.transform;
        this.sync = this.networkedSprite.GetComponent<NetworkedSpriteSync>();
    }

    void Update() {
        var spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        var sprite = spriteRenderer.sprite;

        try {
            if (spriteRenderer.sprite == null) {
                this.sync.spriteID = 0;
            }
            else {
                this.sync.spriteID = Assets.spriteToId[sprite];
            }
        }
        catch (System.Exception ex) {
            print(sprite.name);
            throw ex;
        }

        if (spriteRenderer.material.name.Contains("Sprites-Default")) {
            this.sync.materialID = 0;
        }
        else {
            this.sync.materialID = Assets.GetMaterialID(spriteRenderer.material);
        }

        this.sync.spriteColor = spriteRenderer.color;

        this.sync.transform.position = this.transform.position;
        this.sync.transform.rotation = this.transform.rotation;
        this.sync.transform.localScale = this.transform.lossyScale;
    }

    public void SetFloatProperty(string property, float val) {
        this.sync.floatProperties[property] = val;
    }

    public void SetVectorProperty(string property, Vector4 val) {
        this.sync.vectorProperties[property] = val;
    }

    void OnDestroy() {
        if (this.sync != null) {
            Network.Destroy(this.sync.GetComponent<NetworkView>().viewID);
        }
    }

    void OnEnable() {
        this.sync.isActive = true;
    }

    void OnDisable() {
        this.sync.isActive = false;
    }
}