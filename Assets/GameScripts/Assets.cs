using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;

class Assets {
    private const int rockLayerCount = 3;

    public static List<NationSpriteSet> nationSpriteSets = new List<NationSpriteSet>();
    public static Dictionary<char, Sprite> numbers = new Dictionary<char, Sprite>();
    public static List<Sprite> backgrounds = new List<Sprite>();
    public static List<Sprite> blackholePull;
    public static List<Sprite> blackholePush;
    public static List<Sprite> walls;
    public static Sprite space;
    public static Object spritePrefab;

    public static IDictionary<int, Sprite> idToSprite { get; private set; }
    public static IDictionary<Sprite, int> spriteToId { get; private set; }

    public static IDictionary<string, Sprite> pathToSprite { get; private set; }
    public static Dictionary<string, Object> pathToPrefab = new Dictionary<string, Object>();

    public static Dictionary<int, Material> idToMaterial = new Dictionary<int, Material>();
    private static Dictionary<string, int> materialNameToID = new Dictionary<string, int>();
    private static Dictionary<string, Material> nameToMaterial = new Dictionary<string, Material>();

    public static Dictionary<Sprite, Sprite> spriteToSilhouette = new Dictionary<Sprite, Sprite>();

    private static int spriteIDCounter = 1;
    private static int materialIDCounter = 0;

    static Assets() {
        idToSprite = new Dictionary<int, Sprite>();
        spriteToId = new Dictionary<Sprite, int>();
        pathToSprite = new Dictionary<string, Sprite>();
    }

    public static void LoadAssets() {
        List<Sprite> blockSprites = LoadSpritesAtPath("Blocks");
        List<Sprite> rockSprites = LoadSpritesAtPath("Rocks");
        List<Sprite> numberSprites = LoadSpritesAtPath("Numbers");
        backgrounds = LoadSpritesAtPath("Backgrounds");
        blackholePull = LoadSpritesAtPath("Blackholes/pull");
        blackholePush = LoadSpritesAtPath("Blackholes/push");
        walls = LoadSpritesAtPath("Walls");
        space = (Sprite) Resources.Load("space", typeof(Sprite));
        AddSprite(space);
        spritePrefab = Resources.Load("Prefabs/PlainOldSprite");

        for (int i = 0; i < Main.nationCount; i++)
        {
            nationSpriteSets.Add(new NationSpriteSet());
        }

        foreach (Sprite sprite in blockSprites) {
            int nation = int.Parse(sprite.name[0].ToString()) - 1;
            nationSpriteSets[nation].blocks.Add(sprite);
            AddSprite(sprite);
        }

        foreach (var spriteSet in nationSpriteSets) {
            for (int i = 0; i < rockLayerCount; i++) {
                spriteSet.rockLayers.Add(new List<Sprite>());
            }
        }

        foreach (Sprite sprite in rockSprites) {
            int nation = int.Parse(sprite.name[0].ToString()) - 1;
            int layer = int.Parse(sprite.name[2].ToString()) - 1;
            if (layer == -1) {
                nationSpriteSets[nation].standaloneRocks.Add(sprite);
                AddSprite(sprite);
            }
            else {
                nationSpriteSets[nation].rockLayers[layer].Add(sprite);
                AddSprite(sprite);
            }
        }

        foreach (Sprite sprite in numberSprites.OrderBy((sprite) => sprite.name)) {
            AddSprite(sprite);
            if (sprite.name.Contains("outline")) {
                numbers.Add(sprite.name[0], sprite);
            }
        }

        AddSprites(backgrounds);
        AddSprites(blackholePull);
        AddSprites(blackholePush);
        AddSprites(walls);

        for (int nation = 0; nation < Main.nationCount; nation++) {
            List<Sprite> sprites = Resources.LoadAll("Treasures/nation" + (nation + 1).ToString(), typeof(Sprite)).Cast<Sprite>().ToList();
            int frameIdLength = 1;
            foreach (var group in sprites.GroupBy((sprite) => sprite.name.Substring(0, sprite.name.Length - frameIdLength))) {
                nationSpriteSets[nation].treasureAnimations.Add(group.OrderBy((sprite) => sprite.name).ToList());
                foreach (var sprite in group) {
                    AddSprite(sprite);
                }
            }
        }

        // Get the default sprite material in there
        GameObject temp = new GameObject();
        AddMaterial(temp.AddComponent<SpriteRenderer>().material);
        GameObject.Destroy(temp);

        List<Material> materials = Resources.LoadAll("Materials", typeof(Material)).Cast<Material>().ToList();
        foreach (var material in materials) {
            AddMaterial(material);
        }

        idToSprite[0] = null;

        LoadSpritesAtPath("");
        LoadSpritesAtPath("Text");
        LoadSpritesAtPath("Timer");
        
        numbers['+'] = LoadSpriteAtPath("Numbers/Chars/+_outline");
        numbers['-'] = LoadSpriteAtPath("Numbers/Chars/-_outline");
        numbers[':'] = LoadSpriteAtPath("Numbers/Chars/colonoscopy_outline");

        foreach (var prefab in Resources.LoadAll("Prefabs")) {
            pathToPrefab[prefab.name] = prefab;
        }

        // Silhouettes
        foreach (Sprite silhoutte in Resources.LoadAll("BlockSilhouettes", typeof(Sprite))) {
            Sprite sprite = pathToSprite["Blocks/" + silhoutte.name];
            spriteToSilhouette[sprite] = silhoutte;
        }
    }

    private static List<Sprite> LoadSpritesAtPath(string path) {
        List<Sprite> sprites = Resources.LoadAll(path, typeof(Sprite)).Cast<Sprite>().ToList();
        foreach (Sprite sprite in sprites) {
            AddSprite(sprite);
            pathToSprite[path + "/" + sprite.name] = sprite;
        }

        return sprites;
    }

    private static Sprite LoadSpriteAtPath(string path) {
        Sprite sprite = (Sprite) Resources.Load(path, typeof(Sprite));
        AddSprite(sprite);
        pathToSprite[path + "/" + sprite.name] = sprite;
        return sprite;
    }

    private static void AddSprite(Sprite sprite) {
        idToSprite[spriteIDCounter] = sprite;
        spriteToId[sprite] = spriteIDCounter;
        spriteIDCounter++;
    }

    public static void AddSprites(List<Sprite> sprites) {
        foreach (Sprite sprite in sprites) {
            AddSprite(sprite);
        }
    }

    private static void AddMaterial(Material material) {
        nameToMaterial[material.name] = material;
        materialNameToID[material.name] = materialIDCounter;
        idToMaterial[materialIDCounter] = material;
        materialIDCounter++;
    }

    public static Material GetMaterial(string name) {
        return nameToMaterial[name.Replace(" (Instance)", "")];
    }

    public static int GetMaterialID(Material material) {
        return materialNameToID[material.name.Replace(" (Instance)", "")];
    }
}