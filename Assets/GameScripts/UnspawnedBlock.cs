using UnityEngine;
using System.Collections;

class UnspawnedBlock : MonoBehaviour {
    public const float BLOCK_SPAWNER_ALPHA = .5f;
    
    private float DISTANCE_REQUIRED = Constants.BLOCK_MOVE_DIST_REQUIRED;
    private float FADE_IN_DURATION = .5f;
    private float COUNTER_ALPHA  = .5f;

    private bool isBlockBeingMoved = false;
    private Vector3 mouseOffset;
    private Vector3 initialPosition;

    GameObject counter;

    public Team team;
    public int remainingBlockCount;

    void Awake() {
        counter = Lifecycle.Create("NumberString");
        counter.GetComponent<NumberString>().AllocateDigits(2);
    }

    void Start () {
        initialPosition = transform.position;
        Tiles.OrientUpward(this.transform);

        counter.transform.localScale = new Vector2(6, 6);
        counter.transform.position = this.transform.position.SetZ(Layers.BlockCounter);
        Tiles.OrientUpward(counter.transform);
    }

    public void InitialSpawn() {
        counter.GetComponent<NumberString>().BeginFade(Constants.BUILD_PHASE_INIT_DURATION, 0, COUNTER_ALPHA);
    }

    private void Respawn() {
        counter.GetComponent<NumberString>().BeginFade(0, COUNTER_ALPHA, COUNTER_ALPHA);
    }

    public void SetRemaining (int remain) {
        if (remain <= 0) {
            Destroy(this.gameObject);
            Destroy(counter);
        }

        // counter.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("numbers/" + remain + "_outline");
        counter.GetComponent<NumberString>().number = remain;
        remainingBlockCount = remain;
    }

    void OnTouchDown(UncapturedTouchEvent touch) {
        if (!this.enabled) return;

        touch.Capture(this.gameObject);
        mouseOffset = (Vector3) touch.position - transform.position;
        mouseOffset.z = 0;
        isBlockBeingMoved = true;
        //GameObject.Find("SoundSystem").GetComponent<initSoundSystem>().soundSystem.PlayOneShot("/movePieces/pickup", Vector3.zero);;   // find sound system and attach to it
    }

    void OnTouchDrag(CapturedTouchEvent touch) {
        if (!this.enabled) return;

        if (isBlockBeingMoved) {
            transform.position = (Vector3) touch.position - mouseOffset;
            if (Vector2.Distance(transform.position.XY(), initialPosition.XY()) > DISTANCE_REQUIRED) {

                GameObject unspawnedBlock = (GameObject) Instantiate(this.gameObject);
                unspawnedBlock.transform.position = this.initialPosition;
                unspawnedBlock.GetComponent<Fade>().BeginFade(FADE_IN_DURATION, 0, BLOCK_SPAWNER_ALPHA);

                UnspawnedBlock unspawnedBlockScript = unspawnedBlock.gameObject.GetComponent<UnspawnedBlock>();
                unspawnedBlockScript.SetRemaining(remainingBlockCount - 1);
                unspawnedBlockScript.team = this.team;
                unspawnedBlockScript.Respawn();

                Destroy(this.gameObject.GetComponent<UnspawnedBlock>());
                Destroy(this.counter);
                GhostBlock ghostBlockScript = this.gameObject.AddComponent<GhostBlock>();
                ghostBlockScript.team = team;
                ghostBlockScript.JustSpawned(mouseOffset);

                isBlockBeingMoved = false;
            }
        }
    }

    void OnTouchUp() {
        if (!this.enabled) return;

        isBlockBeingMoved = false;
        transform.position = initialPosition;
    }

    void OnBuildPhaseEnd() {
        counter.GetComponent<NumberString>().BeginFade(Constants.ATTACK_PHASE_INIT_DURATION, BLOCK_SPAWNER_ALPHA, 0);
        GetComponent<Fade>().FadeOutFromCurrent(Constants.ATTACK_PHASE_INIT_DURATION);

        Lifecycle.Release(counter, Constants.ATTACK_PHASE_INIT_DURATION);
        Lifecycle.Release(this.gameObject, Constants.ATTACK_PHASE_INIT_DURATION);
        Destroy(this);
    }
}