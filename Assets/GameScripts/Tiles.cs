using UnityEngine;
using System.Collections.Generic;

class Tiles {
    public const float left = -tileWidth * 3;
    public const float right = tileWidth * 3;
    public const float top = tileHeight / 2;
    public const float bottom = -tileHeight / 2;

    // Currently 10.8, must recalculate and update textures if this is changed
    private const float pixelsToUnits = tileWidthInPixels / tileWidth;

    private const float groundHeightInPixels = 230;
    public static readonly float groundHeight = PixelsToUnits(groundHeightInPixels);

    public const float tileWidth = 100;
    public const float tileHeight = tileWidth * 16 / 9;
    public static Vector2 tileSize = new Vector2(tileWidth, tileHeight);

    public const float tileWidthInPixels = 1080;

    public const float universeUnitWidth = right - left;
    public const float universePixelWidth = 1080 * 6;

    public const float spacePixels = tileWidthInPixels * 4;
    public const float spaceScale = 12;

    private static List<Bounds> mTiles = new List<Bounds>();
    public static IList<Bounds> tiles { get { return mTiles.AsReadOnly(); } }
    public static IList<Bounds> spaceTiles {
        get {
            return mTiles.GetRange(1, mTiles.Count - 2);
        }
    }

    public static Vector2 spaceSize {
        get {
            return new Vector2(tileWidth * (mTiles.Count - 2), tileHeight);
        }
    }


    public static IEnumerable<Bounds> planetTiles {
        get {
            yield return LeftMost;
            yield return RightMost;
        }
    }

    static Tiles() {
        Vector3 size = new Vector3(tileWidth, tileHeight, float.PositiveInfinity);
        for (float x = left; x < right; x += tileWidth) {
            Vector3 center = new Vector3(x + tileWidth / 2, 0, 0);
            mTiles.Add(new Bounds(center, size));
        }
    }

    public static Bounds Get(int index) {
        return mTiles[index];
    }

    public static Bounds LeftMost {
        get {
            return mTiles[0];
        }
    }

    public static Bounds RightMost {
        get {
            return mTiles[mTiles.Count - 1];
        }
    }

    public static Quaternion GetUpwardFacing(Vector3 position) {
        if (LeftMost.Contains(position)) {
            return Quaternion.Euler(0, 0, -90);
        }
        else if (RightMost.Contains(position)) {
            return Quaternion.Euler(0, 0, 90);
        }
        else {
            return Quaternion.identity;
        }
    }

    public static void OrientUpward(Transform transform) {
        transform.rotation = GetUpwardFacing(transform.position);
    }

    public static Vector3 GetRightwardDirection(Vector3 position) {
        if (LeftMost.Contains(position)) {
            return new Vector3(0, -1, 0);
        }
        else if (RightMost.Contains(position)) {
            return new Vector3(0, 1, 0);
        }
        else {
            return new Vector3(1, 0, 0);
        }
    }

    public static Vector3 GetUpwardDirection(Vector3 position) {
        if (LeftMost.Contains(position)) {
            return new Vector3(1, 0, 0);
        }
        else if (RightMost.Contains(position)) {
            return new Vector3(-1, 0, 0);
        }
        else {
            return new Vector3(0, 1, 0);
        }
    }

    public static float PixelsToUnits(float pixels) {
        return pixels / pixelsToUnits;
    }

    public static float UnitsToPixels(float units) {
        return units * pixelsToUnits;
    }
}
