using UnityEngine;

abstract class TouchEvent {
    public Vector2 position { get; private set; }
    public int touchId { get; private set; }

    public TouchEvent(Vector3 position, int touchId) {
        this.position = position;
        this.touchId = touchId;
    }
}