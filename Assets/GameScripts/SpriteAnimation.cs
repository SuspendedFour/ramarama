using UnityEngine;
using System.Collections.Generic;
using System.Collections;

class SpriteAnimation : MonoBehaviour {
    public float fps = 30;
    public List<Sprite> sprites = new List<Sprite>();

    public void Play() {
        StartCoroutine(PlayRoutine());
    }

    private IEnumerator PlayRoutine() {
        float startTime = Time.time;
        float endTime = Time.time + sprites.Count / fps;

        SpriteRenderer renderer = this.GetComponent<SpriteRenderer>();
        while (Time.time < endTime) {
            int index = (int) Mathf.Lerp(0, sprites.Count, (Time.time - startTime) / (endTime - startTime));
            renderer.sprite = sprites[index];
            yield return null;
        }
    }
}