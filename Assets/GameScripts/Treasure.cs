﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

class Treasure : MonoBehaviour {
    private const float SHINE_PERIOD = 10;
    private const float SHINE_MAX_VARIATION = 5;

    public Team team;
    public float health;

    private float initialHealth;
    private List<Sprite> spriteAnimation;

    // Use this for initialization
    void Start () {
        this.transform.position = this.transform.position.SetZ(Layers.Block);
        this.gameObject.layer = LayerMask.NameToLayer("BlocksTeam" + team.teamNumber.ToString());
        this.gameObject.tag = "Treasure";
        this.rigidbody2D.mass = 99999;
        this.rigidbody2D.isKinematic = true;
        Tiles.OrientUpward(this.transform);
        
        var animations = team.spriteSet.treasureAnimations;
        this.spriteAnimation = animations[Random.Range(0, animations.Count)];
        var animScript = this.gameObject.GetComponent<SpriteAnimation>();
        animScript.sprites = spriteAnimation;
        animScript.fps = Constants.SHINE_FPS;

        float mass = Main.spriteToAreaRatio[spriteAnimation[0]] * this.transform.localScale.x;
        this.health = mass * Constants.TREASURE_MASS_TO_HEALTH_FACTOR;
        this.initialHealth = this.health;

        this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteAnimation[0];
        gameObject.AddComponent<PolygonCollider2D>();

        this.team.remainingTreasures++;

        StartCoroutine(ShineRoutine());
    }

    void Update() {
        this.gameObject.GetComponent<HealthbarHost>().healthQuantile = this.health / this.initialHealth;
    }

    private IEnumerator ShineRoutine() {
        while (true) {
            yield return new WaitForSeconds(SHINE_PERIOD + Random.Range(-SHINE_MAX_VARIATION, SHINE_MAX_VARIATION));
            this.GetComponent<SpriteAnimation>().Play();
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Rock") {
            var rock = collision.gameObject.GetComponent<Rock>();
            if (rock == null) return;

            float damage = rock.AttackDamage;
            if (this.team == rock.team) damage *= Constants.FRIENDLY_FIRE_DAMAGE_SCALE;
            health -= damage;

            if (health < 0) {
                var scorePopup = Lifecycle.Create("ScorePopup");
                scorePopup.transform.position = this.transform.position;
                Lifecycle.Release(scorePopup, Constants.SCORE_POPUP_DURATION);

                if (this.team == rock.team) {
                    Scoreboard.AddScore(this.team, -Constants.TREASURE_REWARD);
                    scorePopup.GetComponent<ScorePopup>().Initialize(-Constants.TREASURE_REWARD, this.team.color);
                }
                else {
                    Scoreboard.AddScore(rock.team, Constants.TREASURE_REWARD);
                    scorePopup.GetComponent<ScorePopup>().Initialize(Constants.TREASURE_REWARD, rock.team.color);
                }

                GetComponent<SpriteRenderer>().sprite = spriteAnimation[0];
                var explode = this.gameObject.AddComponent<Explode>();
                explode.ExplodeNow(this.gameObject.GetComponent<SpriteRenderer>().sprite);
                Destroy(this.gameObject);

                team.remainingTreasures--;
                if (team.remainingTreasures == 0) {
                    Team winner = this.team == Team.leftTeam ? Team.rightTeam : Team.leftTeam;
                    Main.GetInstance().EndGame(winner);
                }
            }
        }
    }

    void OnAttackPhaseEnd() {
        Destroy(this.gameObject);
    }
}
