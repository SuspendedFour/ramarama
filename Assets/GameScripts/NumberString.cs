using UnityEngine;
using System.Collections.Generic;

class NumberString : MonoBehaviour {

    public int number;
    public NumberAlignment alignment = NumberAlignment.Center;
    public Color color = Color.white;
    public bool signPrefix;

    private const float DIGIT_SPACING = .7f;

    private List<Transform> numbers = new List<Transform>();

    private static Object prefab;
    public static Object Prefab {
        get {
            if (prefab == null) {
                prefab = Resources.Load("Prefabs/NumberString");
            }

            return prefab;
        }
    }

    void Update() {
        string numberText = number.ToString();
        if (signPrefix) {
            if (this.number > 0) {
                numberText = '+' + numberText;
            }
        }
        // else if (number < 0) {
        //     numberText = numberText.Substring(1);
        // }

        for (int i = 0; i < numberText.Length - numbers.Count; i++) {
            AllocateDigit();
            this.numbers[i].gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }

        float x;
        if (this.alignment == NumberAlignment.Center) {
            x = -(numberText.Length / 2f - .5f) * DIGIT_SPACING;
        }
        else {
            x = 0;
        }

        for (int i = 0; i < numbers.Count; i++) {
            var renderer = this.numbers[i].GetComponent<SpriteRenderer>();

            if (i < numberText.Length) {
                this.numbers[i].renderer.enabled = true;
                this.numbers[i].position = this.transform.TransformPoint(x, 0, 0);
                this.numbers[i].rotation = this.transform.rotation;
                this.numbers[i].localScale = this.transform.lossyScale;

                if (!Assets.numbers.ContainsKey(numberText[i])) print(numberText[i]);

                Sprite sprite = Assets.numbers[numberText[i]];
                if (renderer.sprite != sprite) {
                    renderer.sprite = sprite;
                }

                renderer.color = new Color(this.color.r, this.color.g, this.color.b, renderer.color.a);

                x += DIGIT_SPACING;
            }
            else {
                renderer.enabled = false;
                renderer.SetAlpha(0);
            }
        }
    }

    // Ensure at least this many digits up front so that allocations don't happen during fades
    public void AllocateDigits(int count) {
        for (int i = numbers.Count; i < count; i++) {
            AllocateDigit();
        }
    }

    public void FadeOut(float duration) {
        foreach (var numberObject in this.numbers) {
            numberObject.GetComponent<Fade>().FadeOut(duration);
        }
    }

    public void BeginFade(float duration, float startAlpha, float endAlpha) {
        foreach (var numberObject in this.numbers) {
            numberObject.GetComponent<Fade>().BeginFade(duration, startAlpha, endAlpha);
        }
    }

    public void Show() {
        foreach(var numberObject in this.numbers) {
            numberObject.GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, 1);
        }
    }

    public void Hide() {
        foreach(var numberObject in this.numbers) {
            numberObject.GetComponent<SpriteRenderer>().color *= new Color(1, 1, 1, 0);
        }
    }

    private void AllocateDigit() {
        var t = (Instantiate(Assets.spritePrefab) as GameObject).transform;
        t.name = "NumberChar";
        t.gameObject.AddComponent<Fade>();
        this.numbers.Add(t);
    }

    void OnDestroy() {
        foreach (var number in numbers) {
            if (number != null) {
                Destroy(number.gameObject);
            }
        }
    }
}

enum NumberAlignment {
    Center,
    Left
}