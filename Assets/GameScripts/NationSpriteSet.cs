using UnityEngine;
using System.Collections.Generic;

class NationSpriteSet {
    public List<Sprite> blocks = new List<Sprite>();
    public List<List<Sprite>> rockLayers = new List<List<Sprite>>();
    public List<Sprite> standaloneRocks = new List<Sprite>();
    public List<List<Sprite>> treasureAnimations = new List<List<Sprite>>(); 
}