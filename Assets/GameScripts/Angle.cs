﻿using UnityEngine;
using System.Collections;

public struct Angle {
    private float degrees;
    public float Degrees {
        get {
            return degrees;
        }
    }

    public Angle(float degrees) {
        this.degrees = degrees % 360;
    }

    public bool InBetween(Angle a, Angle b) {
        Angle smaller;
        Angle bigger;

        if (a < b) {
            smaller = a;
            bigger = b;
        }
        else {
            smaller = b;
            bigger = a;
        }
        
        return smaller < this && this < bigger;
    }

    public static bool operator <(Angle a, Angle b) {
        float reciprocalA = (a.Degrees + 180) % 360;
        if (a.Degrees < 180) {
            return b.Degrees > a.Degrees && b.Degrees < reciprocalA;
        }
        else {
            return b.Degrees > a.Degrees || b.Degrees < reciprocalA;
        }
    }

    public static bool operator >(Angle b, Angle a) {
        float reciprocalA = (a.Degrees + 180) % 360;
        if (a.Degrees < 180) {
            return b.Degrees > a.Degrees && b.Degrees < reciprocalA;
        }
        else {
            return b.Degrees > a.Degrees || b.Degrees < reciprocalA;
        }
    }

    public static implicit operator float(Angle a) {
        return a.Degrees;
    }

    public static implicit operator Angle(float a) {
        return new Angle(a);
    }

    public static Angle operator +(Angle a, Angle b) {
        return new Angle(a.Degrees + b.Degrees);
    }

    public override string ToString() {
        return this.Degrees.ToString();
    }
}
