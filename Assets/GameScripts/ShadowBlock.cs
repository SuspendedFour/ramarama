using UnityEngine;

class ShadowBlock : MonoBehaviour {
    private static Object shadowBlockPrefab;
    private GameObject shadowBlock;

    void Start() {
        if (shadowBlockPrefab == null) {
            shadowBlockPrefab = Resources.Load("Prefabs/ShadowBlock");
        }

        this.shadowBlock = (GameObject) Instantiate(shadowBlockPrefab);
        Sprite sprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
        this.shadowBlock.GetComponent<SpriteRenderer>().sprite = sprite;
        this.shadowBlock.AddComponent<PolygonCollider2D>();
        Destroy(this.shadowBlock.GetComponent<SpriteRenderer>());
        this.shadowBlock.transform.parent = this.transform;
    }

    void Update() {
        this.shadowBlock.transform.localPosition = new Vector3(0, 0, Layers.Shadow);
        this.shadowBlock.transform.localScale = new Vector3(1, 1, 1);
        this.shadowBlock.transform.rotation = this.transform.rotation;
    }

    void OnDestroy() {
        if (this.shadowBlock) {
            Destroy(this.shadowBlock);
        }
    }
}