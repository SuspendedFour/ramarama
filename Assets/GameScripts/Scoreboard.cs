using UnityEngine;
using System.Collections.Generic;

class Scoreboard : MonoBehaviour {
    private const float FADE_OUT_SEC = 1;

    private Transform numberObject;
    private NumberString numberScript;
    private NumberString incrementScript;
    private SpriteRenderer colonRenderer;
    private SpriteRenderer scoreRenderer;

    private int score = 0;
    private Team team;

    private static Dictionary<Team, Scoreboard> instances = new Dictionary<Team, Scoreboard>();

    public void SetTeam(Team team) {
        this.team = team;
        instances[team] = this;
    }

    void Start() {
        numberObject = this.transform.Find("Number");
        numberScript = numberObject.GetComponent<NumberString>();
        numberScript.AllocateDigits(2);
        numberScript.Hide();
        incrementScript = this.transform.Find("Increment").GetComponent<NumberString>();
        incrementScript.AllocateDigits(4);
        incrementScript.Hide();

        colonRenderer = this.transform.Find("Colon").GetComponent<SpriteRenderer>();
        scoreRenderer = this.transform.Find("Score").GetComponent<SpriteRenderer>();
    }

    void Update() {
        numberScript.number = score;
    }

    private void AddScore(int delta) {
        incrementScript.number = delta;
        incrementScript.FadeOut(FADE_OUT_SEC);
        score += delta;
        numberScript.signPrefix = score < 0;
    }

    public static void AddScore(Team team, int delta) {
        instances[team].AddScore(delta);
    }

    public static void ResetScores() {
        foreach (var scoreboard in instances.Values) {
            scoreboard.score = 0;
        }
    }

    public void OnBuildPhaseEnd() {
        numberScript.color = team.color;
        incrementScript.color = team.color;
        colonRenderer.color = team.color;
        scoreRenderer.color = team.color;

        numberScript.BeginFade(Constants.ATTACK_PHASE_INIT_DURATION, 0, 1);
        colonRenderer.GetComponent<Fade>().FadeIn(Constants.ATTACK_PHASE_INIT_DURATION);
        scoreRenderer.GetComponent<Fade>().FadeIn(Constants.ATTACK_PHASE_INIT_DURATION);
    }

    public void OnAttackPhaseEnd() {
        numberScript.BeginFade(Constants.ATTACK_PHASE_DESTRUCT_DURATION, 1, 0);
        colonRenderer.GetComponent<Fade>().FadeOut(Constants.ATTACK_PHASE_DESTRUCT_DURATION);
        scoreRenderer.GetComponent<Fade>().FadeOut(Constants.ATTACK_PHASE_DESTRUCT_DURATION);
    }
}