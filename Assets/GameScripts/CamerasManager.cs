using UnityEngine;
using System.Collections.Generic;
using System.Linq;

class CamerasManager : MonoBehaviour {

    private List<Camera> cameras = new List<Camera>();
    private Camera mainCamera;

    private bool dualOutput = false;
    private bool swapOutputs = false;
    private int viewNumber = 0;

    void Awake() {
        for (int i = 0; i < 6; i++) {
            GameObject camObj = new GameObject("Camera " + i.ToString());
            var cam = camObj.AddComponent<Camera>();
            this.cameras.Add(cam);

            cam.orthographic = true;
			cam.rect = new Rect(0, 0, 1, 1);
			cam.backgroundColor = Color.black;
            cam.orthographicSize = Tiles.tileWidth / 2;
            cam.transform.position = Tiles.tiles[i].center.SetZ(Layers.Camera);
        }

        this.mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        this.mainCamera.orthographicSize = Tiles.tileHeight * 1.25f;
        SetCameraView(0);
    }

    private void SetCameraView(int viewNumber) {
        this.viewNumber = viewNumber;
        UpdateCams();
    }

    private void UpdateCams() {
        ResetCams();

        if (viewNumber == 0) {
            this.mainCamera.rect = new Rect(0, .5f, 1, .5f);
            this.mainCamera.enabled = true;
            var leftCam = cameras.First();
            leftCam.rect = new Rect(0, 0, .5f, .5f);
            leftCam.enabled = true;
            var rightCam = cameras.Last();
            rightCam.rect = new Rect(.5f, 0, .5f, .5f);
            rightCam.enabled = true;
            rightCam.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else if (viewNumber == 9) {
            var topCam = cameras.First();
            var bottomCam = cameras.Last();

            if (swapOutputs) {
                topCam = cameras.Last();
                bottomCam = cameras.First();
            }

            topCam.enabled = true;
            bottomCam.enabled = true;
            topCam.rect = new Rect(0, 0, 1f, .5f);
            bottomCam.rect = new Rect(0, .5f, 1f, .5f);
        }
        else {
            int index = (viewNumber - 1);
            if (dualOutput && index + 1 < cameras.Count) {
                Camera bottomCam;
                Camera topCam;

                if (swapOutputs) {
                    bottomCam = cameras[index + 1];
                    topCam = cameras[index];
                }
                else {
                    bottomCam = cameras[index];
                    topCam = cameras[index + 1];
                }

                topCam.enabled = true;
                bottomCam.enabled = true;
                topCam.rect = new Rect(0, 0, 1f, .5f);
                bottomCam.rect = new Rect(0, .5f, 1f, .5f);
            }
            else {
                cameras[index].enabled = true;
            }
        }
    }

    private void ResetCams() {
        foreach (Camera cam in this.cameras) {
            cam.transform.rotation = Quaternion.Euler(0, 0, -90);
            cam.enabled = false;
			cam.rect = new Rect(0, 0, 1, 1);
        }

		for (int i = 0; i < this.cameras.Count; i++) {
			if (i < this.cameras.Count / 2) {
			    this.cameras[i].transform.rotation = Quaternion.Euler(0, 0, -90);
			}
			else {
				this.cameras[i].transform.rotation = Quaternion.Euler(0, 0, 90);
			}
		}

        this.mainCamera.enabled = false;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha0)) SetCameraView(0);
        if (Input.GetKeyDown(KeyCode.Alpha1)) SetCameraView(1);
        if (Input.GetKeyDown(KeyCode.Alpha2)) SetCameraView(2);
        if (Input.GetKeyDown(KeyCode.Alpha3)) SetCameraView(3);
        if (Input.GetKeyDown(KeyCode.Alpha4)) SetCameraView(4);
        if (Input.GetKeyDown(KeyCode.Alpha5)) SetCameraView(5);
        if (Input.GetKeyDown(KeyCode.Alpha6)) SetCameraView(6);
        if (Input.GetKeyDown(KeyCode.Alpha9)) SetCameraView(9);

        if (Input.GetKeyDown(KeyCode.Q)) {
            dualOutput = false;
            UpdateCams();
        }
        
        if (Input.GetKeyDown(KeyCode.W)) {
            dualOutput = true;
            UpdateCams();
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            swapOutputs = !swapOutputs;
            UpdateCams();
        }
    }
}