using UnityEngine;
using System;

class UncapturedTouchEvent : TouchEvent {
    private Action<GameObject> onCapture;

    public UncapturedTouchEvent(Vector3 position, int touchId, Action<GameObject> onCapture) : base(position, touchId) {
        this.onCapture = onCapture;
    }

    public void Capture(GameObject gameObject) {
        if (onCapture != null) {
            onCapture(gameObject);
        }
    }
}