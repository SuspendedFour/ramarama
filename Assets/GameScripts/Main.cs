﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

using Random = UnityEngine.Random;

class Main : MonoBehaviour {

    private static Main singleton;
    private bool forceStart = false; //is button is pressed, go into attack mode

    // Resources
    public Transform blockPrefab = null;
    public Transform groundPrefab = null;
    public Transform unspawnedBlockPrefab = null;
    public Transform rockPrefab = null;

    public GameObject leftTimer;
    public GameObject rightTimer;
    private GameObject leftPhaseText;
    private GameObject leftPhaseText2;
    private GameObject rightPhaseText;
    private GameObject rightPhaseText2;

    private GameObject leftEndGameText;
    private GameObject rightEndGameText;

    private GameObject leftTitle;
    private GameObject rightTitle;

    private GameObject leftShotsLeft;
    private GameObject rightShotsLeft;

    //public GameObject leftTreasures;
    //public GameObject rightTreasures;

    private GameObject planet0;
    private GameObject planet1;
    private List<GameObject> walls = new List<GameObject>();

    // Constants
    private float left  = Tiles.left;
    private float right = Tiles.right;
    private float top = Tiles.top;
    private float bottom = Tiles.bottom;

    public static int nationCount = 4;
    private int buildPhaseSeconds = Constants.BUILD_TIME;
    public static float blockSize = Constants.BLOCK_SIZE;
    public static float rockSize = Constants.ROCK_SIZE;

    // State
    public static Dictionary<Sprite, float> spriteToAreaRatio = new Dictionary<Sprite, float>();
    public static GamePhase gamePhase;

    // Use this for initialization
    void Awake () {
        Assets.LoadAssets();
        NetworkMain.instance = (Network.Instantiate(Assets.pathToPrefab["NetworkMain"], Vector3.zero, Quaternion.identity, 0) as GameObject).networkView;

        // Setup title screen
        leftTitle = Lifecycle.Create("PlainOldSprite");
        leftTitle.name = "Left Title";
        leftTitle.AddComponent<CrossFade>();
        leftTitle.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/title"];
        leftTitle.transform.position = Tiles.LeftMost.center.SetZ(Layers.TitleScreen);
        leftTitle.transform.localScale = new Vector3(9.5f, 9.5f, 1);
        Tiles.OrientUpward(leftTitle.transform);

        rightTitle = Lifecycle.Create("PlainOldSprite");
        rightTitle.name = "Right Title";
        rightTitle.AddComponent<CrossFade>();
        rightTitle.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/title"];
        rightTitle.transform.position = Tiles.RightMost.center.SetZ(Layers.TitleScreen);
        rightTitle.transform.localScale = new Vector3(9.5f, 9.5f, 1);
        Tiles.OrientUpward(rightTitle.transform);

        // Setup planets
        float textureWidthInPixels = 1650f;
        float textureWidthInUnits = Tiles.PixelsToUnits(textureWidthInPixels);
        float planetAdjustOffset = Tiles.PixelsToUnits(30);

        planet0 = Instantiate(Assets.spritePrefab) as GameObject;
        planet0.name = "Left Planet";
        planet0.transform.localPosition = new Vector3(Tiles.LeftMost.min.x + textureWidthInUnits / 2 - planetAdjustOffset, Tiles.LeftMost.center.y, Layers.Planets);
        planet0.transform.localScale = new Vector3(8.9f, 8.9f, 1);
        Tiles.OrientUpward(planet0.transform);
        planet0.AddComponent<CrossFade>();
        planet0.layer = LayerMask.NameToLayer("NoCollide");

        planet1 = Instantiate(Assets.spritePrefab) as GameObject;
        planet1.name = "Right Planet";
        planet1.transform.localPosition = new Vector3(Tiles.RightMost.max.x - textureWidthInUnits / 2 + planetAdjustOffset, Tiles.RightMost.center.y, Layers.Planets);
        planet1.transform.localScale = new Vector3(8.9f, 8.9f, 1);
        Tiles.OrientUpward(planet1.transform);
        planet1.AddComponent<CrossFade>();
        planet1.layer = LayerMask.NameToLayer("NoCollide");

        // Setup space
        GameObject space = (GameObject) Instantiate(Assets.spritePrefab);
        space.name = "Space";
        space.transform.localPosition = new Vector3(0, 0, Layers.Space);
        space.transform.localScale = new Vector3(Tiles.spaceScale, Tiles.spaceScale, 1);
        space.GetComponent<SpriteRenderer>().sprite = Assets.space;
        space.renderer.material = Assets.GetMaterial("Blackhole");
        space.renderer.material.SetFloat("_AspectRatio", (float) Assets.space.texture.width / Assets.space.texture.height);
        Blackhole.spaceNetworkSprite = space.GetComponent<NetworkedSprite>();
        Blackhole.spaceMaterial = space.renderer.material;

        // Setup walls
        float wallThickness = Wall.WALL_THICKNESS;

        for (float x = -Tiles.tileWidth * 4; x <= Tiles.tileWidth * 4; x += Tiles.tileWidth * 4) {
            var wallTop = Instantiate(Resources.Load("Prefabs/Wall")) as GameObject;
            wallTop.transform.position = new Vector3(x, Tiles.top - wallThickness / 2, Layers.Walls);
            wallTop.transform.rotation = Quaternion.Euler(0, 0, 90);

            var wallBottom = Instantiate(Resources.Load("Prefabs/Wall")) as GameObject;
            wallBottom.transform.position = new Vector3(x, Tiles.bottom + wallThickness / 2, Layers.Walls);
            wallBottom.transform.rotation = Quaternion.Euler(0, 0, -90);

            walls.Add(wallTop);
            walls.Add(wallBottom);
        }

        var xList = new List<float>();
        var yList = new List<float>();
        xList.Add(Tiles.LeftMost.center.x);
        xList.Add(Tiles.RightMost.center.x);
        yList.Add(Tiles.top - wallThickness / 2);
        yList.Add(Tiles.bottom + wallThickness / 2);

        foreach (float x in xList) {
            foreach (float y in yList) {
                var wall = new GameObject();
                wall.transform.position = new Vector3(x, y, 0);
                wall.layer = LayerMask.NameToLayer("PlanetWalls");
                wall.name = "Planet Wall";

                var body = wall.AddComponent<Rigidbody2D>();
                body.isKinematic = true;

                var box = wall.AddComponent<BoxCollider2D>();
                box.size = new Vector2(Tiles.tileWidth, wallThickness);
            }
        }

        // Setup ground
        float groundHeightInUnits = Tiles.groundHeight;
		float groundLength = 9999999;

        Transform leftGround = (Transform) Instantiate(groundPrefab);
        leftGround.gameObject.tag = "Ground";
        leftGround.localPosition = new Vector3(left + groundHeightInUnits / 2 - planetAdjustOffset, 0, 0);
        leftGround.localScale = new Vector3(groundHeightInUnits, groundLength, 1);

        Transform rightGround = (Transform) Instantiate(groundPrefab);
        rightGround.gameObject.tag = "Ground";
        rightGround.localPosition = new Vector3(right - groundHeightInUnits / 2 + planetAdjustOffset, 0, 0);
        rightGround.localScale = new Vector3(groundHeightInUnits, groundLength, 1);

        // Setup planet colliders
        GameObject leftPlanetCollider = (GameObject) Instantiate(Resources.Load("Prefabs/PlanetCollider"));
        leftPlanetCollider.transform.position = Tiles.LeftMost.center.SetZ(Layers.PlanetCollider);
        leftPlanetCollider.transform.localScale = new Vector3(Tiles.tileWidth, Tiles.tileHeight, 1);

        GameObject rightPlanetCollider = (GameObject) Instantiate(Resources.Load("Prefabs/PlanetCollider"));
        rightPlanetCollider.transform.position = Tiles.RightMost.center.SetZ(Layers.PlanetCollider);
        rightPlanetCollider.transform.localScale = new Vector3(Tiles.tileWidth, Tiles.tileHeight, 1);

        // Setup timers
        leftTimer = new GameObject("SexyTimer");
        leftTimer.AddComponent<SexyTimer>();
        leftTimer.transform.position = new Vector3(left + Tiles.tileWidth - 10, 0, Layers.Text);
        leftTimer.transform.rotation = Tiles.GetUpwardFacing(leftTimer.transform.position);
        leftTimer.GetComponent<SexyTimer>().color = new Color(1, 1, 1, 0);

        rightTimer = new GameObject("SexyTimer");
        rightTimer.AddComponent<SexyTimer>();
        rightTimer.transform.position = new Vector3(right - Tiles.tileWidth + 10, 0, Layers.Text);
        rightTimer.transform.rotation = Tiles.GetUpwardFacing(rightTimer.transform.position);
        rightTimer.GetComponent<SexyTimer>().color = new Color(1, 1, 1, 0);

        // Setup phase text
        float phaseTextScale = 8;

        leftPhaseText = Lifecycle.Create("PlainOldSprite");
        leftPhaseText.AddComponent<Fade>();
        leftPhaseText.transform.position = new Vector3(Tiles.LeftMost.center.x, 0, Layers.Text);
        leftPhaseText.transform.localScale = new Vector3(phaseTextScale, phaseTextScale, 1);
        Tiles.OrientUpward(leftPhaseText.transform);
        leftPhaseText2 = Lifecycle.Create("PlainOldSprite");
        leftPhaseText2.AddComponent<Fade>();
        leftPhaseText2.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/phase_outline"];
        leftPhaseText2.GetComponent<SpriteRenderer>().SetAlpha(0);
        leftPhaseText2.transform.position = new Vector3(Tiles.LeftMost.center.x, 0, Layers.Text);
        leftPhaseText2.transform.localScale = new Vector3(phaseTextScale, phaseTextScale, 1);
        Tiles.OrientUpward(leftPhaseText2.transform);

        rightPhaseText = Lifecycle.Create("PlainOldSprite");
        rightPhaseText.AddComponent<Fade>();
        rightPhaseText.transform.position = new Vector3(Tiles.RightMost.center.x, 0, Layers.Text);
        rightPhaseText.transform.localScale = new Vector3(phaseTextScale, phaseTextScale, 1);
        Tiles.OrientUpward(rightPhaseText.transform);
        rightPhaseText2 = Lifecycle.Create("PlainOldSprite");
        rightPhaseText2.AddComponent<Fade>();
        rightPhaseText2.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/phase_outline"];
        rightPhaseText2.GetComponent<SpriteRenderer>().SetAlpha(0);
        rightPhaseText2.transform.position = new Vector3(Tiles.RightMost.center.x, 0, Layers.Text);
        rightPhaseText2.transform.localScale = new Vector3(phaseTextScale, phaseTextScale, 1);
        Tiles.OrientUpward(rightPhaseText2.transform);

        // Setup end game text
        float endGameTextScale = 8;

        leftEndGameText = Lifecycle.Create("PlainOldSprite");
        leftEndGameText.AddComponent<Fade>();
        leftEndGameText.transform.position = Tiles.LeftMost.center.SetZ(Layers.Text);
        leftEndGameText.transform.localScale = new Vector3(endGameTextScale, endGameTextScale, 1);
        Tiles.OrientUpward(leftEndGameText.transform);

        rightEndGameText = Lifecycle.Create("PlainOldSprite");
        rightEndGameText.AddComponent<Fade>();
        rightEndGameText.transform.position = Tiles.RightMost.center.SetZ(Layers.Text);
        rightEndGameText.transform.localScale = new Vector3(endGameTextScale, endGameTextScale, 1);
        Tiles.OrientUpward(rightEndGameText.transform);

        // Setup scoreboards
        var scoreboardPrefab = Resources.Load("Prefabs/Scoreboard");
        float textMargin = 15;
        float textScale = 7;

        var leftScoreboard = (GameObject) Instantiate(scoreboardPrefab);
        leftScoreboard.transform.position = new Vector3(Tiles.LeftMost.min.x + Tiles.groundHeight - textMargin, Tiles.LeftMost.max.y - textMargin, Layers.Text);
        leftScoreboard.transform.localScale = new Vector3(textScale, textScale, 0);
        leftScoreboard.GetComponent<Scoreboard>().SetTeam(Team.teams[0]);
        Tiles.OrientUpward(leftScoreboard.transform);

        var rightScoreboard = (GameObject) Instantiate(scoreboardPrefab);
        rightScoreboard.transform.position = new Vector3(Tiles.RightMost.max.x - Tiles.groundHeight + textMargin, Tiles.RightMost.min.y + textMargin, Layers.Text);
        rightScoreboard.transform.localScale = new Vector3(textScale, textScale, 0);
        rightScoreboard.GetComponent<Scoreboard>().SetTeam(Team.teams[1]);
        Tiles.OrientUpward(rightScoreboard.transform);

        // Setup shots left
        leftShotsLeft = Lifecycle.Create("ShotsLeft");
        leftShotsLeft.transform.position = new Vector3(Tiles.left + Tiles.groundHeight - textMargin, 0, Layers.Text);
        leftShotsLeft.transform.localScale = new Vector3(textScale, textScale, 1);
        Tiles.OrientUpward(leftShotsLeft.transform);
        leftShotsLeft.GetComponent<ShotsLeft>().team = Team.leftTeam;

        rightShotsLeft = Lifecycle.Create("ShotsLeft");
        rightShotsLeft.transform.position = new Vector3(Tiles.right - Tiles.groundHeight + textMargin, 0, Layers.Text);
        rightShotsLeft.transform.localScale = new Vector3(textScale, textScale, 1);
        Tiles.OrientUpward(rightShotsLeft.transform);
        rightShotsLeft.GetComponent<ShotsLeft>().team = Team.rightTeam;

        PrecomputeRockBlockAreas();
        StartCoroutine(StartTitleScreen());
    }

    private void PrecomputeRockBlockAreas() {
        foreach (NationSpriteSet spriteSet in Assets.nationSpriteSets) {
            foreach (Sprite sprite in spriteSet.blocks) {
                PrecomputeSpriteArea(sprite);
            }

            foreach (Sprite sprite in spriteSet.standaloneRocks) {
                PrecomputeSpriteArea(sprite);
            }

            foreach (var animation in spriteSet.treasureAnimations) {
                PrecomputeSpriteArea(animation[0]);
            }

            foreach (var layer in spriteSet.rockLayers) {
                foreach (var sprite in layer) {
                    PrecomputeSpriteArea(sprite);
                }
            }
        }
    }

    private void PrecomputeSpriteArea(Sprite sprite) {
        Color[] colors = sprite.texture.GetPixels();
        float nonTransparentPixelCount = colors.Where((color) => color.a != 0).Count();
        float nonTransparentPixelRatio = nonTransparentPixelCount / colors.Length;
        spriteToAreaRatio.Add(sprite, nonTransparentPixelRatio);
    }

    private IEnumerator<YieldInstruction> StartTitleScreen() {
        float titleFadeDuration = 2;

        leftTitle.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/title"];
        leftTitle.GetComponent<Fade>().FadeIn(titleFadeDuration);
        rightTitle.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/title"];
        rightTitle.GetComponent<Fade>().FadeIn(titleFadeDuration);

        yield return new WaitForSeconds(Constants.TITLE_TIME);

        leftTitle.GetComponent<CrossFade>().BeginFade(Assets.pathToSprite["Text/sponsors"], titleFadeDuration);
        rightTitle.GetComponent<CrossFade>().BeginFade(Assets.pathToSprite["Text/sponsors"], titleFadeDuration);

        yield return new WaitForSeconds(Constants.SPONSORS_TIME);

        leftTitle.GetComponent<Fade>().FadeOut(titleFadeDuration);
        rightTitle.GetComponent<Fade>().FadeOut(titleFadeDuration);

        StartCoroutine(StartBuildPhase());
    }

    private IEnumerator<YieldInstruction> StartBuildPhase()
    {
        SendToAll("OnBuildPhaseStart");

        gamePhase = GamePhase.Build;
        InitializeTeams();
        generateTreasures();
        generateBlackholes();
        generateWalls();

        float textFadeInDuration = 2;
        float textFadeOutDuration = 1;
        leftPhaseText.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/build_outline"];
        leftPhaseText.GetComponent<SpriteRenderer>().color = Team.leftTeam.color;
        leftPhaseText.GetComponent<Fade>().FadeIn(textFadeInDuration);
        leftPhaseText.GetComponent<Fade>().FadeOut(Constants.BUILD_PHASE_INIT_DURATION - textFadeOutDuration, textFadeOutDuration);

        leftPhaseText2.GetComponent<SpriteRenderer>().color = Team.leftTeam.color;
        leftPhaseText2.GetComponent<Fade>().FadeIn(textFadeInDuration);
        leftPhaseText2.GetComponent<Fade>().FadeOut(Constants.BUILD_PHASE_INIT_DURATION - textFadeOutDuration, textFadeOutDuration);

        rightPhaseText.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/build_outline"];
        rightPhaseText.GetComponent<SpriteRenderer>().color = Team.rightTeam.color;
        rightPhaseText.GetComponent<Fade>().FadeIn(textFadeInDuration);
        rightPhaseText.GetComponent<Fade>().FadeOut(Constants.BUILD_PHASE_INIT_DURATION - textFadeOutDuration, textFadeOutDuration);

        rightPhaseText2.GetComponent<SpriteRenderer>().color = Team.rightTeam.color;
        rightPhaseText2.GetComponent<Fade>().FadeIn(textFadeInDuration);
        rightPhaseText2.GetComponent<Fade>().FadeOut(Constants.BUILD_PHASE_INIT_DURATION - textFadeOutDuration, textFadeOutDuration);

        Scoreboard.ResetScores();

        yield return new WaitForSeconds(Constants.BUILD_PHASE_INIT_DURATION);

        leftTimer.GetComponent<SexyTimer>().color = Team.leftTeam.color.SetAlpha(0);
        rightTimer.GetComponent<SexyTimer>().color = Team.rightTeam.color.SetAlpha(0);
        leftTimer.GetComponent<SexyTimer>().BeginFade(SexyTimer.FADE_TIME, 0, 1);
        rightTimer.GetComponent<SexyTimer>().BeginFade(SexyTimer.FADE_TIME, 0, 1);

        for (int countdown = buildPhaseSeconds; countdown > 0 && !forceStart; countdown--)
        {
            var timespan = TimeSpan.FromSeconds(countdown);
            string countdownText = string.Format("{0:00}:{1:00}", timespan.Minutes, timespan.Seconds);
            
            leftTimer.GetComponent<SexyTimer>().SetTime(countdownText);
            rightTimer.GetComponent<SexyTimer>().SetTime(countdownText);

            if (countdown < 4)
            {
                //GameObject.Find("SoundSystem").GetComponent<initSoundSystem>().soundSystem.PlayOneShot("/countdown/shortbuzz", Vector3.zero);   // find sound system and attach to it
            }
            yield return new WaitForSeconds(1);
        }

        forceStart = false;

        SendToAll("OnBuildPhaseEnd");
        //GameObject.Find("SoundSystem").GetComponent<initSoundSystem>().soundSystem.PlayOneShot("/countdown/start", Vector3.zero);   // find sound system and attach to it
        //GameObject.Find("SoundSystem").GetComponent<initSoundSystem>().soundSystem.PlayOneShot("/countdown/longbuzz", Vector3.zero);   // find sound system and attach to it
        StartCoroutine(StartAttackPhase());
    }

    private void InitializeTeams() {
        Team leftTeam = Team.teams[0];
        Team rightTeam = Team.teams[1];

        int leftTeamNation = Random.Range(0, nationCount);
        int rightTeamNation;
        do {
            rightTeamNation = Random.Range(0, nationCount);
        } while (leftTeamNation == rightTeamNation);

        leftTeam.nation = leftTeamNation;
        leftTeam.spriteSet = Assets.nationSpriteSets[leftTeamNation];
        leftTeam.teamNumber = 0;
        leftTeam.shotsLeft = Constants.MAX_SHOTS;
        rightTeam.nation = rightTeamNation;
        rightTeam.spriteSet = Assets.nationSpriteSets[rightTeamNation];
        rightTeam.teamNumber = 1;
        rightTeam.shotsLeft = Constants.MAX_SHOTS;

        float leftTeamHue = Random.Range(0f, 360f);
        float rightTeamHue = (leftTeamHue + 180f) % 360f;
        leftTeam.color = Utils.ColorFromHSV(leftTeamHue, .7f, 1f);
        rightTeam.color = Utils.ColorFromHSV(rightTeamHue, .7f, 1f);

        int totalBlocksThisRound = Random.Range(Constants.BLOCKS_PER_TEAM_MIN, Constants.BLOCKS_PER_TEAM_MAX);

        CreateSpawners(left + 80, leftTeam, totalBlocksThisRound);
        CreateSpawners(right - 80, rightTeam, totalBlocksThisRound);

        Sprite sprite0 = Assets.backgrounds[leftTeam.nation];
        Sprite sprite1 = Assets.backgrounds[rightTeam.nation];

        float planetFadeDuration = 4;
        planet0.GetComponent<CrossFade>().BeginFade(sprite0, planetFadeDuration);
        planet1.GetComponent<CrossFade>().BeginFade(sprite1, planetFadeDuration);

        planet0.AddOrGetComponent<PolygonCollider2D>();
        planet1.AddOrGetComponent<PolygonCollider2D>();
    }

    public void CreateSpawners(float x, Team team, int blocks) {
        int[] blockCounts = assignBlockCounts(blocks);
        int blockPointCounter = 0;
        List<Sprite> nationSprites = team.spriteSet.blocks;
        IEnumerator<Sprite> spriteIterator = nationSprites.GetEnumerator();
        spriteIterator.MoveNext();
        float spriteWidth = (top - bottom) / nationSprites.Count;
        for (float y = bottom + spriteWidth / 2; y < top; y += spriteWidth)
        {
            Sprite sprite = spriteIterator.Current;
            spriteIterator.MoveNext();

            GameObject blockSpawner = (Instantiate(unspawnedBlockPrefab) as Transform).gameObject;
            SpriteRenderer renderer = (SpriteRenderer)blockSpawner.renderer;

            // Load the silhoutte to initialize the collider, and then load the actual sprite
            renderer.sprite = Assets.spriteToSilhouette[sprite];
            blockSpawner.AddComponent("PolygonCollider2D");
            renderer.sprite = sprite;

            UnspawnedBlock unspawnedBlockScript = blockSpawner.GetComponent<UnspawnedBlock>();
            unspawnedBlockScript.team = team;
            unspawnedBlockScript.SetRemaining(blockCounts[blockPointCounter++]);
            unspawnedBlockScript.InitialSpawn();

            blockSpawner.transform.position = new Vector3(x, y, Layers.UnspawnedBlock);
            blockSpawner.transform.localScale = new Vector3(blockSize, blockSize, 1);
            blockSpawner.GetComponent<Fade>().BeginFade(Constants.BUILD_PHASE_INIT_DURATION, 0, UnspawnedBlock.BLOCK_SPAWNER_ALPHA);
            StartCoroutine(EnableScriptAfterDelay(unspawnedBlockScript, Constants.BUILD_PHASE_INIT_DURATION));
        }
    }

    int[] assignBlockCounts(int counts) {
        int[] returnMe = {0,0,0,0,0,0};
        while (counts > 0) {
            returnMe[Random.Range(0, 6)]++;
            --counts;
        }
        return returnMe;
    }

    private void generateTreasures() {
        int treasureCount = Random.Range(Constants.TREASURE_MIN_COUNT, Constants.TREASURE_MAX_COUNT + 1);

        Vector2 leftTeamRectMin = new Vector2(Tiles.LeftMost.min.x + Tiles.groundHeight, Tiles.bottom);
        Vector2 leftTeamRectMax = new Vector2(Tiles.LeftMost.min.x + Constants.TREASURE_MAX_HEIGHT, Tiles.top);
        List<Vector2> leftTeamPositions = Utils.PlaceSquaresInRectOrDie(treasureCount, Constants.TREASURE_MARGINS, leftTeamRectMin, leftTeamRectMax);

        foreach (Vector2 pos in leftTeamPositions) {
            GameObject treasure = Lifecycle.Create("Treasure");
            treasure.transform.position = pos.SetZ(Layers.Block);
            treasure.transform.localScale = new Vector3(Constants.TREASURE_SIZE, Constants.TREASURE_SIZE, 1);
            Tiles.OrientUpward(treasure.transform);
            treasure.GetComponent<Treasure>().team = Team.leftTeam;
            treasure.GetComponent<Fade>().FadeIn(Constants.BUILD_PHASE_INIT_DURATION);
        }

        Vector2 rightTeamRectMin = new Vector2(Tiles.RightMost.max.x - Constants.TREASURE_MAX_HEIGHT, Tiles.bottom);
        Vector2 rightTeamRectMax = new Vector2(Tiles.RightMost.max.x - Tiles.groundHeight, Tiles.top);
        List<Vector2> rightTeamPositions = Utils.PlaceSquaresInRectOrDie(treasureCount, Constants.TREASURE_MARGINS, rightTeamRectMin, rightTeamRectMax);

        foreach (Vector2 pos in rightTeamPositions) {
            GameObject treasure = Lifecycle.Create("Treasure");
            treasure.transform.position = pos.SetZ(Layers.Block);
            treasure.transform.localScale = new Vector3(Constants.TREASURE_SIZE, Constants.TREASURE_SIZE, 1);
            Tiles.OrientUpward(treasure.transform);
            treasure.GetComponent<Treasure>().team = Team.rightTeam;
            treasure.GetComponent<Fade>().FadeIn(Constants.BUILD_PHASE_INIT_DURATION);
        }
    }

    private void generateBlackholes()
    {
        Blackhole.ResetHoles();
        int numberOfHoles = Random.Range(Constants.NUM_BLACK_HOLES_MIN, Constants.NUM_BLACK_HOLES_MAX+1);
        for (int i = 0; i < numberOfHoles; i++)
        {
            GameObject hole = Instantiate(Resources.Load("Prefabs/Blackhole")) as GameObject;
            float xRange = Random.Range(Tiles.left + Tiles.tileWidth, Tiles.right - Tiles.tileWidth);
            float yRange = Random.Range(Tiles.bottom + Tiles.tileHeight, Tiles.top - Tiles.tileHeight);
            hole.transform.position = new Vector2(xRange, yRange);    //TODO make not spawn DIRECTLY on another hole
            hole.GetComponent<Blackhole>().blackholeId = i;
        }
    }

    private void generateWalls()
    {
        int wallType = Random.Range(0, Wall.NUM_TYPES_OF_WALLS);
        foreach (var wall in walls) {
            wall.GetComponent<Wall>().setWallType(wallType);
        }
    }

    private IEnumerator<YieldInstruction> StartAttackPhase() {
        gamePhase = GamePhase.Attack;
        leftTimer.GetComponent<SexyTimer>().BeginFade(SexyTimer.FADE_TIME, 1, 0);
        rightTimer.GetComponent<SexyTimer>().BeginFade(SexyTimer.FADE_TIME, 1, 0);

        float textFadeInDuration = 1;
        float textFadeOutDuration = 1;
        float textDuration = Constants.ATTACK_PHASE_INIT_DURATION;
        leftPhaseText.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/attack_outline"];
        leftPhaseText.GetComponent<SpriteRenderer>().color = Team.leftTeam.color;
        leftPhaseText.GetComponent<Fade>().FadeIn(textFadeInDuration);
        leftPhaseText.GetComponent<Fade>().FadeOut(textDuration - textFadeOutDuration, textFadeOutDuration);

        leftPhaseText2.GetComponent<SpriteRenderer>().color = Team.leftTeam.color;
        leftPhaseText2.GetComponent<Fade>().FadeIn(textFadeInDuration);
        leftPhaseText2.GetComponent<Fade>().FadeOut(textDuration - textFadeOutDuration, textFadeOutDuration);

        rightPhaseText.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/attack_outline"];
        rightPhaseText.GetComponent<SpriteRenderer>().color = Team.rightTeam.color;
        rightPhaseText.GetComponent<Fade>().FadeIn(textFadeInDuration);
        rightPhaseText.GetComponent<Fade>().FadeOut(textDuration - textFadeOutDuration, textFadeOutDuration);

        rightPhaseText2.GetComponent<SpriteRenderer>().color = Team.rightTeam.color;
        rightPhaseText2.GetComponent<Fade>().FadeIn(textFadeInDuration);
        rightPhaseText2.GetComponent<Fade>().FadeOut(textDuration - textFadeOutDuration, textFadeOutDuration);

        yield return null;
    }

    private void FadeIn(GameObject obj, float maxDuration) {
        Fade fadeScript = obj.GetComponent<Fade>();
        float duration = 1f;
        float phase = Random.Range(0, maxDuration - duration);
        fadeScript.FadeIn(phase, duration);
    }

    private IEnumerator EnableScriptAfterDelay(MonoBehaviour script, float delay) {
        // This frame is required by UnspawnedBlock to orient itself in Start() before we turn it off
        yield return null;
        if (script == null) yield break;

        script.enabled = false;
        yield return new WaitForSeconds(delay);
        script.enabled = true;
    }

    public void EndGame(Team winner) {
        var leftRenderer = leftEndGameText.GetComponent<SpriteRenderer>();
        var rightRenderer = rightEndGameText.GetComponent<SpriteRenderer>();

        if (winner == Team.teams[0]) {
            leftRenderer.sprite = Assets.pathToSprite["Text/win_outline"];
            rightRenderer.sprite = Assets.pathToSprite["Text/lose_outline"];
        }
        else {
            leftRenderer.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/lose_outline"];
            rightRenderer.GetComponent<SpriteRenderer>().sprite = Assets.pathToSprite["Text/win_outline"];
        }

        leftRenderer.color = Team.leftTeam.color;
        rightRenderer.color = Team.rightTeam.color;
        float textFadeOutDuration = 1;
        leftEndGameText.GetComponent<Fade>().FadeOut(Constants.ATTACK_PHASE_DESTRUCT_DURATION - textFadeOutDuration, textFadeOutDuration);
        rightEndGameText.GetComponent<Fade>().FadeOut(Constants.ATTACK_PHASE_DESTRUCT_DURATION - textFadeOutDuration, textFadeOutDuration);

        StartCoroutine(EndGameRoutine(winner));
    }

    private IEnumerator<YieldInstruction> EndGameRoutine(Team winner)
    {
        SendToAll("OnAttackPhaseEnd");
        gamePhase = GamePhase.PostAttack;

        yield return new WaitForSeconds(Constants.ATTACK_PHASE_DESTRUCT_DURATION);
        StartCoroutine(StartTitleScreen());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f"))
        {
            forceStart = true;  //force scene into attack mode
        }
        if (Input.GetKeyDown("g"))
        {
            EndGame(Team.leftTeam);
        }

        // if (TuioInput.touches.Length > 0) {
        //     print("Update");
        //     foreach (var touch in TuioInput.touches) {
        //         print("position: " + touch.position);
        //         print("raw: " + touch.rawPosition);
        //     }

        //     print("");
        // }
    }

    public static Main GetInstance() {
        return singleton;
    }

    public Main() {
        singleton = this;
    }

    public void SendToAll(string message) {
        foreach (var obj in GameObject.FindObjectsOfType<GameObject>()) {
            if (obj.activeInHierarchy) {
                obj.SendMessage(message, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
