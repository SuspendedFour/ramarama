using UnityEngine;
using System.Collections;

class CrackAnimation : MonoBehaviour {
    // (Doesn't actually start pulsing until damaged)
    private const float MAX_HP_PULSE_PERIOD = 4;
    private const float PULSE_DURATION = .25f;

    public float healthQuantile;
    private float healthQuantileLastFrame;

    private float targetTime;

    void Update() {
        if (healthQuantile != healthQuantileLastFrame) {
            StopCoroutine("CrackRoutine");

            if (healthQuantile == 1) {
                SetSeed(0);
            }
            else {
                StartCoroutine("CrackRoutine");
            }

            healthQuantileLastFrame = healthQuantile;
        }
    }

    IEnumerator CrackRoutine() {
        float pulseDuration = PULSE_DURATION;

        float startPulsePeriod = MAX_HP_PULSE_PERIOD;
        float finalPulsePeriod = pulseDuration;

        float pulsePeriod = Mathf.Lerp(startPulsePeriod, finalPulsePeriod, 1 - healthQuantile);

        while (true) {
            // Do the micro pulses
            float microPulseFinishTime = Time.time + pulseDuration;

            while (Time.time < microPulseFinishTime) {
                SetSeed(Random.value);
                yield return null;
            }

            SetSeed(0);
            yield return new WaitForSeconds(pulsePeriod);
        }
    }

    // Seed of 0 means no distortion
    private void SetSeed(float seed) {
       this.renderer.material.SetFloat("_Seed", seed);
       this.GetComponent<NetworkedSprite>().SetFloatProperty("_Seed", seed);
   }
}