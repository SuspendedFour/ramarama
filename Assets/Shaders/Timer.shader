﻿// Shader that does nothing, for reference

Shader "Custom/Timer" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Angle ("Angle (0-1)", float) = 80
    }

    SubShader {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
        // Blend One One
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 200

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Angle;

            struct a2v
            {
                float4 vertex : POSITION;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
            };

            v2f vert (a2v v)
            {
                v2f o;
                o.pos = mul( UNITY_MATRIX_MVP, v.vertex);
                o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
                return o;
            }

            float4 frag(v2f i) : COLOR
            {
                float4 result = tex2D (_MainTex, i.uv);

                float2 xy = i.uv - float2(.5, .5);
                float theta = atan(xy.y / xy.x);
                if (xy.x < 0) theta += radians(180.0);
                theta -= radians(90.0);
                if (theta < 0) theta += radians(360.0);

                // Add .05 to give user satisfaction of complete circle
                if (theta > (1 - (_Angle + .05)) * radians(360.0)) {
                    result.g -= .8;
                    result.b -= .8;
                }

                return result;
            }

            ENDCG
        }
    }

    FallBack "Diffuse"
}