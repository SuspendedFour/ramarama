﻿Shader "Custom/Crack" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Seed ("Seed", float) = 0
    }

    SubShader {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 200

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Seed;

            struct a2v
            {
                float4 vertex : POSITION;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
            };

            float rand(float3 co)
            {
                return frac(sin( dot(co.xyz ,float3(12.9898,78.233,45.5432) )) * 43758.5453);
            }

            v2f vert (a2v v)
            {
                v2f o;
                o.pos = mul( UNITY_MATRIX_MVP, v.vertex);
                o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
                return o;
            }

            float4 frag(v2f i) : COLOR
            {
                float4 result = tex2D (_MainTex, i.uv);

                if (_Seed != 0 && result.a > .5) {
                    float3 seedVector = float3(i.uv.x, i.uv.y, _Seed);
                    result.a = rand(seedVector) / 2 + .5;
                }
                
                return result;
            }

            ENDCG
        }
    }

    FallBack "Diffuse"
}