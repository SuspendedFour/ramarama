﻿ Shader "Custom/shader2" {
	Properties {
		
	  Intensity("intensity", Range(0,8)) = 6.0
	  GlowGamma("Centrality", Range(.5,2)) = 1.6
	  BlurStart("Blur Start", Range(0,1)) = 1.0
	  BlurWidth("Blur Width", Range(-10,10)) = -.3
	  CX("CX", Range(-1,2)) = 0.5
      CY("CY", Range(-1,2)) = 0.5
      ColorSampler ("Base (RGB)", 2D) = "white" {}
      _MainTex ("Texture", 2D) = "white" {}
      

	}
	SubShader {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		Pass {
		//Blend SrcAlpha Zero //Zero//OneMinusSrcAlpha
	
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200
		
		CGPROGRAM

     //   #pragma exclude_renderers gles
        #pragma target 3.0
		#pragma vertex vert
		#pragma fragment frag
		

	    float Intensity;
	    float GlowGamma;
	    float BlurStart;
	    float BlurWidth;
	    float CX;
        float CY;
		sampler2D ColorSampler;
		sampler2D _MainTex;
	
		
		
		struct appdata {
   			float4 Position	: POSITION;
       		float2 TexCoord		: TEXCOORD0;
        }; 
        
        struct QuadVertexOutput {
			float4 Position	: POSITION;
    	 	float2 UV	: TEXCOORD0;
        
        };

////////////////////////////////////////////////////////////
////////////////////////////////// vertex shaders //////////
////////////////////////////////////////////////////////////

QuadVertexOutput vert(appdata IN)
{
    QuadVertexOutput OUT;
    OUT.Position = IN.Position;
    float2 ctrPt = float2(CX,CY);
    OUT.UV = (IN.TexCoord - ctrPt);
    
    OUT.Position =  mul(UNITY_MATRIX_MVP, IN.Position);
  //  OUT.UV = mul(_Object2World, OUT.UV);
    //output.positionWorld = mul(_Object2World, input.vertex);
    return OUT;
}

//////////////////////////////////////////////////////
////////////////////////////////// pixel shaders /////
//////////////////////////////////////////////////////

half4 frag(QuadVertexOutput IN

				//uniform sampler2D tex,
				//uniform float BlurStart,
				//uniform float BlurWidth,
				//uniform float CX,
				//uniform float CY,
				//uniform float Intensity,
				//uniform float GlowGamma,
				//uniform int nsamples
) : COLOR {
    half4 blurred = 0;
    float2 ctrPt = float2(CX,CY);
    // this loop will be unrolled by compiler and the constants precalculated:
    for(int i = 0; i < 32; i++) {
    	float scale = BlurStart + BlurWidth*(i/(float)32);
    	blurred += tex2D(_MainTex, IN.UV*scale + ctrPt );
    }
    
    blurred /= 32;
    blurred.rgb = pow(blurred.rgb,GlowGamma);
    blurred.rgb *= Intensity;
    blurred.rgb = saturate(blurred.rgb);
    half4 origTex = tex2D(_MainTex, IN.UV + ctrPt );
    half3 newC = origTex.rgb + (1.0-origTex.a)* blurred.rgb;
    half newA = max(origTex.a,blurred.a);

    
    
    return half4(newC.rgb,newA);
} 
		ENDCG
	} 
	}
	FallBack "Diffuse"
}
