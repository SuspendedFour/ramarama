// Shader that does nothing, for reference

Shader "Custom/Blackhole" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _AspectRatio ("Texture Aspect Ratio", float) = 2.160337

        // radius in uv space
        _Radius ("Radius", float) = .1

        // normalized position, z and w components ignored
        _Position0 ("Position0", Vector) = (0, 0, 0, 0)
        // color added to pixels
        _Color0 ("Color0", Color) = (.2, 0, 0, 0)

        _Position1 ("Position1", Vector) = (0, 0, 0, 0)
        _Position2 ("Position2", Vector) = (0, 0, 0, 0)
        _Position3 ("Position3", Vector) = (0, 0, 0, 0)
        _Position4 ("Position4", Vector) = (0, 0, 0, 0)

        _Color1 ("Color1", Color) = (.2, 0, 0, 0)
        _Color2 ("Color2", Color) = (.2, 0, 0, 0)
        _Color3 ("Color3", Color) = (.2, 0, 0, 0)
        _Color4 ("Color4", Color) = (.2, 0, 0, 0)

        _Color1 ("Color1", Color) = (.2, 0, 0, 0)
        _Color2 ("Color2", Color) = (.2, 0, 0, 0)
        _Color3 ("Color3", Color) = (.2, 0, 0, 0)
        _Color4 ("Color4", Color) = (.2, 0, 0, 0)

		_TimeElapsed0 ("Time Elapsed0", float) = 0
        _TimeElapsed1 ("Time Elapsed1", float) = 0
        _TimeElapsed2 ("Time Elapsed2", float) = 0
        _TimeElapsed3 ("Time Elapsed3", float) = 0
        _TimeElapsed4 ("Time Elapsed4", float) = 0
    }

    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200

        Pass {
            CGPROGRAM
            #pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float _AspectRatio;
            float _Radius;

            float4 _Position0;
            float4 _Position1;
            float4 _Position2;
            float4 _Position3;
            float4 _Position4;
            float4 _Color0;
            float4 _Color1;
            float4 _Color2;
            float4 _Color3;
            float4 _Color4;
            float _TimeElapsed0;
            float _TimeElapsed1;
            float _TimeElapsed2;
            float _TimeElapsed3;
            float _TimeElapsed4;

            // temporary variable required by TRANSFORM_TEX
            float4 _MainTex_ST;

            struct a2v
            {
                float4 vertex : POSITION;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
                float4 pos2: TEXCOORD1;
            };

            float mod(float x, float y) {
                return x - y * floor(x / y);
            }

            v2f vert (a2v v)
            {
                v2f o;
                o.pos = mul( UNITY_MATRIX_MVP, v.vertex);
                o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
                o.pos2 = mul(_Object2World, v.vertex);
                return o;
            }

            float4 applyBlackhole(v2f i, float2 position, float4 color, float time)
            {
                float maxRadius = _Radius;
                float maxFrequency = .05;
                float stepInterval = .005;
                float4 result;

                float2 xy = i.uv - float2(.5, .5) - position;
                xy.x *= _AspectRatio;

                float r = length(xy);
                
                float theta = atan(xy.y / xy.x);
                if (xy.x < 0) theta += radians(180.0);

                float freqLerp = r / maxRadius;
                float frequency = lerp(maxFrequency, 0, freqLerp);
                if (frequency != 0) {
                    float period = 1 / frequency;
                    float t = mod(time, period);
                    theta += lerp(0, radians(360.0), t / period);
                }

                float2 xyFinal = float2(r * cos(theta), r * sin(theta));
                xyFinal += float2(.5, .5) + position;
                xyFinal.x /= _AspectRatio;

                result = tex2D (_MainTex, xyFinal);
                result += lerp(color, float4(0, 0, 0, 0), r / maxRadius);

                if (r > _Radius) {
                    result = float4(0, 0, 0, 0);
                }

                return result;
            }

            float4 frag(v2f i) : COLOR
            {
                float4 result = float4(0, 0, 0, 0);
                float4 result0 = applyBlackhole(i, _Position0.xy, _Color0, _TimeElapsed0);
                float4 result1 = applyBlackhole(i, _Position1.xy, _Color1, _TimeElapsed1);
                float4 result2 = applyBlackhole(i, _Position2.xy, _Color2, _TimeElapsed2);
                float4 result3 = applyBlackhole(i, _Position3.xy, _Color3, _TimeElapsed3);
                float4 result4 = applyBlackhole(i, _Position4.xy, _Color4, _TimeElapsed4);

                int count = 0;
                if (result0.x != 0 || result0.y != 0 || result0.z != 0 || result0.w != 0) {
                    count++;
                }

                if (result1.x != 0 || result1.y != 0 || result1.z != 0 || result1.w != 0) {
                    count++;
                }

                if (result2.x != 0 || result2.y != 0 || result2.z != 0 || result2.w != 0) {
                    count++;
                }

                if (result3.x != 0 || result3.y != 0 || result3.z != 0 || result3.w != 0) {
                    count++;
                }

                if (result4.x != 0 || result4.y != 0 || result4.z != 0 || result4.w != 0) {
                    count++;
                }

                result += result0;
                result += result1;
                result += result2;
                result += result3;
                result += result4;
                result /= count;

                float4 pure = tex2D (_MainTex, i.uv);
                if (result.x == 0 && result.y == 0 && result.z == 0 && result.w == 0) {
                    return pure;
                }

                return result;
            }

            ENDCG
        }
    }

    FallBack "Diffuse"
}