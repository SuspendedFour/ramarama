﻿using UnityEngine;
using System.Collections;
using FMOD.Studio;

public class initSoundSystem : MonoBehaviour {
    public FMOD_StudioSystem soundSystem;
    public FMOD_StudioSystem soundSystem2;

	// Use this for initialization
	void Start () {
        //Cache the FMOD_StudioSystem instance for easier access
        soundSystem = FMOD_StudioSystem.instance;
        soundSystem2 = FMOD_StudioSystem.instance;
        //soundSystem.PlayOneShot("asd",Vector3());

        //FMOD.Channel poo;
        //FMOD.ChannelGroup poop;
        //FMOD.Event e;
        //FMOD.Event_Factory ef;
        //FMOD.EventQueue eq;
        //FMOD.System sys2 = new FMOD.System();
        //FMOD.Factory.System_Create(ref sys2);

        //FMOD.System sys = new FMOD.System();
        //sys.setSoftwareFormat(44100, FMOD.SPEAKERMODE.RAW, 8);
        //sys.set3DSpeakerPosition
        //soundSystem.
        //sys.
        
        //poo.setSpeakerLevels(FMOD.SPEAKER.MONO,

        //declare a string variable holding the file name. The Application.dataPath is needed so that
        //the sounds work both in the editor and in standalone builds
        string fileName = Application.dataPath + "/StreamingAssets/Master Bank.bank";

        //Load the FMOD bank
        FMOD.Studio.Bank bank;
        FMOD.Studio.Bank bank2;
        //FMOD_StudioSystem.ERRCHECK(
            soundSystem.System.loadBankFile(fileName, out bank);//);
            soundSystem2.System.loadBankFile(fileName, out bank2);//);
        //Also load the corresponding strings bank so that we can find the events by their names
        FMOD.Studio.Bank bankStrings;
        //FMOD_StudioSystem.ERRCHECK(
        soundSystem.System.loadBankFile(fileName + ".strings", out bankStrings);
            //);
        Debug.Log("Sound system loaded.");
	}

    void Awake()
    {
        
    }
    	
	// Update is called once per frame
	void Update () {
        Vector3 pos = new Vector3(-3, 0, Shmang(-20)); //sound is at x=20 relative to listener
        Vector3 up = new Vector3(0, 1, 0);  //y is up
        Vector3 vel = new Vector3(0, 0, 0);  // not moving
        _3D_ATTRIBUTES my3dPos = new _3D_ATTRIBUTES();
        my3dPos.position = FMOD.Studio.UnityUtil.toFMODVector(pos);
        my3dPos.up = FMOD.Studio.UnityUtil.toFMODVector(up);
        my3dPos.velocity = FMOD.Studio.UnityUtil.toFMODVector(vel);
        soundSystem.System.setListenerAttributes(my3dPos);
	}

    float Shmang(int input)
    {
        return (input + 190F) / (190F * 2F);
    }

    void OnDestroy()
    {
        //soundSystem.System.release();
        //soundSystem2.System.release();
    }
}
