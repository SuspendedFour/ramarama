using System;
using System.Threading;
using NUnit.Framework;

namespace UnityTest
{
    internal class RotationControllerTests
    {
        [Test]
        public void Functional()
        {
            RotationController rotationController = new RotationController();
            rotationController.StartRotation();
            
            // Go full circle
            rotationController.ProcessInput(0);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(0, rotationController.Z.Degrees);

            rotationController.ProcessInput(45);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(45, rotationController.Z.Degrees);

            rotationController.ProcessInput(185);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(185, rotationController.Z.Degrees);

            rotationController.ProcessInput(350);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(350, rotationController.Z.Degrees);

            rotationController.ProcessInput(5);
            Assert.True(rotationController.IsReflected);
            Assert.AreEqual(5, rotationController.Z.Degrees);

            // Go backward
            rotationController.ProcessInput(350);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(350, rotationController.Z.Degrees);

            // Go forward
            rotationController.ProcessInput(5);
            Assert.True(rotationController.IsReflected);
            Assert.AreEqual(5, rotationController.Z.Degrees);

            // Go back to start
            rotationController.ProcessInput(350);
            rotationController.ProcessInput(180);
            rotationController.ProcessInput(5);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(5, rotationController.Z.Degrees);

            // Pass through start
            rotationController.ProcessInput(350);
            Assert.True(rotationController.IsReflected);
            Assert.AreEqual(350, rotationController.Z.Degrees);
        }

        [Test]
        public void CounterClockwise()
        {
            RotationController rotationController = new RotationController();
            rotationController.StartRotation();
            rotationController.ProcessInput(0);

            rotationController.ProcessInput(350);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(350, rotationController.Z.Degrees);

            rotationController.ProcessInput(181);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(181, rotationController.Z.Degrees);

            rotationController.ProcessInput(5);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(5, rotationController.Z.Degrees);

            // Cross, counter clockwise
            rotationController.ProcessInput(350);
            Assert.True(rotationController.IsReflected);
            Assert.AreEqual(350, rotationController.Z.Degrees);

            // Cross back over, clockwise
            rotationController.ProcessInput(5);
            Assert.False(rotationController.IsReflected);
            Assert.AreEqual(5, rotationController.Z.Degrees);
        }
    }
}
