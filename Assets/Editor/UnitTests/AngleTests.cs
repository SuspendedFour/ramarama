﻿using System;
using System.Threading;
using NUnit.Framework;

namespace UnityTest
{
    internal class AngleTests
    {
        [Test]
        public void LessThan()
        {
            Angle a = new Angle(10);
            Angle b = new Angle(350);

            Assert.True(b < a);
            Assert.False(a < b);

            Angle c = new Angle(180);
            Angle d = new Angle(181);
            Assert.True(c < d);
            Assert.False(d < c);

            Angle e = new Angle(175);
            Angle f = new Angle(185);
            Assert.True(e < f);
            Assert.False(f < e);

            Angle g = new Angle(350);
            Angle h = new Angle(180);
            Assert.True(h < g);
            Assert.False(g < h);

            Angle i = new Angle(5);
            Angle j = new Angle(180);
            Assert.True(i < j);
        }

        [Test]
        public void InBetween()
        {
            Angle a = new Angle(350);
            Angle b = new Angle(10);
            Angle c = new Angle(180);
            Angle d = new Angle(0);
            Angle e = new Angle(5);
            Assert.False(c.InBetween(a, b));
            Assert.False(c.InBetween(b, a));
            Assert.False(a.InBetween(b, c));
            Assert.False(b.InBetween(a, c));

            Assert.True(d.InBetween(a, b));
            Assert.False(a.InBetween(b, d));
            Assert.False(b.InBetween(a, d));

            Assert.True(b.InBetween(e, c));
        }

        [Test]
        public void Cast()
        {
            Angle a = new Angle(350);
            a += 180;
            Assert.AreEqual(170, (float) a);
        }

        [Test]
        public void Add()
        {
            Angle a = 350;
            Angle b = 20;
            Assert.AreEqual(10, (a + b).Degrees);
        }
    }
}
